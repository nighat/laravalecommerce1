<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Product::class, 5)->create()->each(function ($product) {
            $product->review()->save(factory(App\Review::class)->make());
            $product->picture()->save(factory(App\Picture::class)->make());

        });

    }
}
