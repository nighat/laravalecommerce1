<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProfessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Profession::class, 5)->create();
    }
}
