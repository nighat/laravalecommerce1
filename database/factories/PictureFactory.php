<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
$factory->define(App\Picture::class, function (Faker $faker) {
    return [

        'path'       => $faker->word,
        'type'       => $faker->word,
        'created_by'       => $faker->word,
        'updated_by'       => $faker->word,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];
});
