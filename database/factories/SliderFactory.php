<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
$factory->define(App\Slider::class, function (Faker $faker) {
    return [


        'image' =>$faker->image(public_path('uploads/slider-image'), 200, 200, 'nature', false),
        //'picture' => $faker->image(public_path('uploads'),400,300,'cats',false),
        'content' => $faker->text,
        'link' => $faker->url,
        'html_blog' => $faker->text,
        'created_by'       => $faker->word,
        'updated_by'       => $faker->word,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];
});
