<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'title'       => $faker->title,
        'short_description'       => $faker->text,
        'description'       => $faker->text,
        'additional_information'       => $faker->text,
        'price'       => $faker->randomDigit,
        'special_price'       => $faker->randomDigit,
        'start_date'       => $faker->date,
        'end_date'       => $faker->date,
        'sku'       => $faker->word,
        'meta_keyword'       => $faker->word,
        'meta_description'       => $faker->text,
        'product_url'       => $faker->url,
        'created_by'       => $faker->word,
        'updated_by'       => $faker->word,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];
});
