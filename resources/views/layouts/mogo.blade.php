<!DOCTYPE html>
<html lang="en-US" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="keywords" content="html5 Template">
    <meta name="description" content="Mogo - Responsive html5 Template">
    <meta name="author" content="etheme.com">
    <link rel="shortcut icon" href="{{ asset('images/logo2.png') }}">
    <title>Shop Store</title>
    <base href="/">

    <!-- STYLESHEET -->
    <!-- FONTS -->
    <!-- Muli -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800%7CMontserrat:300,400,500,600,700%7COpen+Sans">

    <!-- icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('fonts/icons/fontawesome/font-awesome.css') }}">
    <!-- MyFont -->
    <link rel="stylesheet" href="{{ asset('fonts/icons/myfont/css/myfont.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/icons/myfont/css/myfont-embedded.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/icons/myfont/css/animation.css') }}">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{ asset('fonts/icons/myfont/css/myfont-ie7.css')}}">
    <![endif]-->

    <!-- Vendor -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/v3/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/v4/css/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/perfectScrollbar/css/perfect-scrollbar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/tonymenu/css/tonymenu.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/revolution/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/revolution/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/revolution/css/navigation.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/slick/slick.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/magnificPopup/dist/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/rangeSlider/css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" href="{{asset('vendor/rangeSlider/css/ion.rangeSlider.skinFlat.css')}}}">
    <link rel="stylesheet" href="{{ asset('vendor/swiper/css/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fotorama/fotorama.css') }}">


    <!-- Custom -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- html5 shim and Respond.js IE8 support of html5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body id="theme">
<!-- HEADER -->
<header>
    <div class="tt-preloader"></div>

    <div class="tt-header tt-header--build-01 tt-header--style-01 tt-header--sticky">
        <div class="tt-header__content">
            <div class="tt-header__logo">
                <div class="h1 tt-logo tt-logo__curtain">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('images/ecomerce.png') }}" alt="mogo">
                    </a>
                </div>
            </div>
            <div class="tt-header__nav">
                <div class="tt-header__menu">
                    <nav class="TonyM TonyM--header"
                         data-tm-dir="row"
                         data-tm-mob="true"
                         data-tm-anm="emersion">
                        <ul class="TonyM__panel">
                            <li>
                                <a href="{{ route('home') }}">
                                    Home
                                    {{--<i class="TonyM__arw"></i>--}}
                                </a>
                                {{--<div class="TonyM__mm TonyM__mm--simple"--}}
                                     {{--data-tm-w="280"--}}
                                     {{--data-tm-a-h="item-left">--}}
                                    {{--<ul class="TonyM__list">--}}
                                        {{--<li><a href="{{ route('home') }}">Home</a></li>--}}
                                        {{--<li><a href="index-02">Home — Variant 2</a></li>--}}
                                        {{--<li><a href="index-03.">Home — Variant 3</a></li>--}}
                                        {{--<li><a href="index-04">Home — Variant 4</a></li>--}}
                                        {{--<li><a href="index-05">Home — Variant 5</a></li>--}}
                                        {{--<li><a href="index-06">Home — Variant 6</a></li>--}}
                                        {{--<li><a href="index-07">Home — Variant 7</a></li>--}}
                                        {{--<li><a href="index-08">Home — Variant 8</a></li>--}}
                                        {{--<li><a href="index-09">Home — Variant 9</a></li>--}}
                                        {{--<li><a href="index-10">Home — Variant 10</a></li>--}}
                                        {{--<li><a href="index-11">Home — Variant 11</a></li>--}}
                                        {{--<li><a href="index-12">Home — Variant 12</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            </li>
                            <li>
                                <a href="{{ route('product.index') }}">
                                    Product

                                </a>

                            </li>
                            <li>
                                <a href="{{ route('sliders.index') }}">
                                    Slider

                                </a>

                            </li>
                            <li>
                                <a href="{{ route('category.index') }}">
                                    Category
                                </a>

                            </li>
                            <li>
                                <a href="{{ route('staticblock.index') }}">
                                    Static Block
                                </a>

                            </li>

                            <li>
                                <a href="{{ route('review.index') }}">
                                   Review
                                </a>

                            </li>
                            <li>
                                <a href="{{ route('picture.index') }}">
                                   Picture
                                </a>

                            </li>
                            <li>
                                <a href="{{ route('product.view') }}">
                                    <span>
                                        Shop Now
                                        {{--<i class="TonyM__arw"></i>--}}
                                    </span>
                                </a>
                                {{--<div class="TonyM__mm TonyM__mm--simple"--}}
                                     {{--data-tm-w="1400"--}}
                                     {{--data-tm-a-h="menu-left">--}}
                                    {{--<ul class="TonyM__list TonyM--gr5-in">--}}
                                        {{--<li class="TonyM__ttl">--}}
                                            {{--<a href="listing-with-custom-html-block">Listing</a>--}}
                                            {{--<ul>--}}
                                                {{--<li><a href="listing-collections">Listing Collections</a></li>--}}
                                                {{--<li><a href="listing-with-custom-html-block">Listing with Custom html Block</a></li>--}}
                                                {{--<li><a href="listing-catalogue">Listing Catalogue</a></li>--}}
                                                {{--<li><a href="listing-with-sidebar">Listing with Sidebar</a></li>--}}
                                                {{--<li><a href="listing-right-sidebar">Listing Right Sidebar</a></li>--}}
                                                {{--<li><a href="listing-without-columns">Listing without Columns</a></li>--}}
                                                {{--<li><a href="listing-fullwidth">Listing Fullwidth</a></li>--}}
                                                {{--<li><a href="listing-big-reviews">Listing Big Previews</a></li>--}}
                                                {{--<li><a href="listing">Listing Show All</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}
                                        {{--<li class="TonyM__ttl">--}}
                                            {{--<a href="product-simple-variant-1">Product</a>--}}
                                            {{--<ul>--}}
                                                {{--<li><a href="product-simple-variant-1" >Simple Product - Variant 1</a></li>--}}
                                                {{--<li><a href="product-simple-variant-2">Simple Product - Variant 2</a></li>--}}
                                                {{--<li><a href="product-variable">Variable Product</a></li>--}}
                                                {{--<li><a href="product-grouped">Grouped product</a></li>--}}
                                                {{--<li><a href="product-out-of-stock">Out of stock product</a></li>--}}
                                                {{--<li><a href="product-on-sale">On sale product</a></li>--}}
                                                {{--<li><a href="product">Product All</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}
                                        {{--<li class="TonyM__ttl">--}}
                                            {{--<a href="index">Pages</a>--}}
                                            {{--<ul>--}}
                                                {{--<li><a href="my-account">My Account</a></li>--}}
                                                {{--<li><a href="compare">Compare</a></li>--}}
                                                {{--<li><a href="wishlist">Wishlist</a></li>--}}
                                                {{--<li><a href="checkout-01">Checkout Variant #1</a></li>--}}
                                                {{--<li><a href="checkout-02">Checkout Variant #2</a></li>--}}
                                                {{--<li><a href="cart-01">Cart Variant #1</a></li>--}}
                                                {{--<li><a href="cart-02">Cart Variant #2</a></li>--}}
                                                {{--<li><a href="contacts">Contacts</a></li>--}}
                                                {{--<li><a href="about">About</a></li>--}}
                                                {{--<li><a href="faq">FAQ</a></li>--}}
                                                {{--<li><a href="sitemap">Sitemap</a></li>--}}
                                                {{--<li><a href="services">Services</a></li>--}}
                                                {{--<li><a href="coming-soon">Coming Soon</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}
                                        {{--<li class="TonyM__ttl">--}}
                                            {{--<a href="404.html">Empty Pages</a>--}}
                                            {{--<ul>--}}
                                                {{--<li><a href="404.html">404</a></li>--}}
                                                {{--<li><a href="empty-category">Empty Category</a></li>--}}
                                                {{--<li><a href="empty-compare">Empty Compare</a></li>--}}
                                                {{--<li><a href="empty-search">Empty Search</a></li>--}}
                                                {{--<li><a href="empty-shopping-cart">Empty Shopping-cart</a></li>--}}
                                                {{--<li><a href="empty-wishlist">Empty Wishlist</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}
                                        {{--<li class="TonyM__ttl">--}}
                                            {{--<a href="index">Elements</a>--}}
                                            {{--<ul class="TonyM__list_inner TonyM--gr2-in">--}}
                                                {{--<li><a href="elements.typography" >Typography</a></li>--}}
                                                {{--<li><a href="elements.banners">Banners</a></li>--}}
                                                {{--<li><a href="elements.tabs">Tabs</a></li>--}}
                                                {{--<li><a href="elements.accordion">Accordion</a></li>--}}
                                                {{--<li><a href="elements.toggles">FAQs / Toggles</a></li>--}}
                                                {{--<li><a href="elements.social">Social Media Profiles</a></li>--}}
                                                {{--<li><a href="elements.buttons">Buttons</a></li>--}}
                                                {{--<li><a href="elements.products">Products</a></li>--}}
                                                {{--<li><a href="elements.icon-box">Icon Box</a></li>--}}
                                                {{--<li><a href="elements.progress-bar">Progress Bars</a></li>--}}
                                                {{--<li><a href="elements.maps">Google Maps</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            </li>

                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--Collections--}}
                                    {{--<i class="TonyM__arw"></i>--}}
                                {{--</a>--}}
                                {{--<div class="TonyM__mm TonyM__mm--pdg-only-c"--}}
                                     {{--data-tm-w="1800"--}}
                                     {{--data-tm-a-h="window-center">--}}
                                    {{--<div class="TonyM__bx-out-c">--}}
                                        {{--<div class="TonyM__bx-c TonyM--gr6-c4">--}}
                                            {{--<div class="TonyM__bx-in-c">--}}
                                                {{--<ul class="TonyM__list">--}}
                                                    {{--<li class="TonyM__ttl TonyM--gr12-c3">--}}
                                                        {{--<a href="#">--}}
                                                            {{--<span class="TonyM__ttl-img">--}}
                                                                {{--Audio--}}
                                                                {{--<span><img src="images/megamenu/collections/megamenu-collection-01.jpg" alt="Image name"></span>--}}
                                                            {{--</span>--}}
                                                        {{--</a>--}}
                                                        {{--<ul>--}}
                                                            {{--<li><a href="#">Wireless</a></li>--}}
                                                            {{--<li class="TonyM__dd">--}}
                                                                {{--<a href="#">--}}
                                                                    {{--<span>--}}
                                                                        {{--Built-In Microphone--}}
                                                                        {{--<span class="TonyM__label TonyM__label--sale">SALE</span>--}}
                                                                    {{--</span>--}}
                                                                {{--</a>--}}
                                                                {{--<ul>--}}
                                                                    {{--<li><a href="#">Built-In - Item #1</a></li>--}}
                                                                    {{--<li><a href="#">Built-In - Item #2</a></li>--}}
                                                                    {{--<li><a href="#">Built-In - Item #3</a>--}}
                                                                        {{--<ul>--}}
                                                                            {{--<li><a href="#">Built-In - Item #3.1</a></li>--}}
                                                                            {{--<li><a href="#">Built-In - Item #3.2</a></li>--}}
                                                                            {{--<li><a href="#">Built-In - Item #3.3</a></li>--}}
                                                                            {{--<li><a href="#">Built-In - Item #3.4</a></li>--}}
                                                                        {{--</ul>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li><a href="#">Built-In Item - #4</a></li>--}}
                                                                {{--</ul>--}}
                                                            {{--</li>--}}
                                                            {{--<li><a href="#">Bluetooth Enabled</a></li>--}}
                                                            {{--<li><a href="#">Rechargeable</a></li>--}}
                                                            {{--<li><a href="#">USB Device Charging</a></li>--}}
                                                        {{--</ul>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="TonyM__ttl TonyM--gr12-c3">--}}
                                                        {{--<a href="#">--}}
                                                            {{--<span class="TonyM__ttl-img">--}}
                                                                {{--Watches--}}
                                                                {{--<span><img src="images/megamenu/collections/megamenu-collection-02.jpg" alt="Image name"></span>--}}
                                                            {{--</span>--}}
                                                        {{--</a>--}}
                                                        {{--<ul>--}}
                                                            {{--<li><a href="#">Clock Display</a></li>--}}
                                                            {{--<li>--}}
                                                                {{--<a href="#">--}}
                                                                    {{--<span>--}}
                                                                        {{--Water Resistant--}}
                                                                        {{--<span class="TonyM__label TonyM__label--new">NEW</span>--}}
                                                                    {{--</span>--}}
                                                                {{--</a>--}}
                                                            {{--</li>--}}
                                                            {{--<li><a href="#">Wireless Syncing</a></li>--}}
                                                            {{--<li>--}}
                                                                {{--<a href="#">--}}
                                                                    {{--<span>--}}
                                                                        {{--Rechargeable--}}
                                                                        {{--<span class="TonyM__label TonyM__label--sale">SALE</span>--}}
                                                                    {{--</span>--}}
                                                                {{--</a>--}}
                                                            {{--</li>--}}
                                                            {{--<li><a href="#">GPS Enabled</a></li>--}}
                                                        {{--</ul>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="TonyM__ttl TonyM--gr12-c3">--}}
                                                        {{--<a href="#">--}}
                                                            {{--<span class="TonyM__ttl-img">--}}
                                                                {{--Trackers--}}
                                                                {{--<span><img src="images/megamenu/collections/megamenu-collection-03.jpg" alt="Image name"></span>--}}
                                                            {{--</span>--}}
                                                        {{--</a>--}}
                                                        {{--<ul>--}}
                                                            {{--<li><a href="#">Watch style</a></li>--}}
                                                            {{--<li><a href="#">Wrist</a></li>--}}
                                                            {{--<li><a href="#">--}}
                                                {{--<span>--}}
                                                    {{--Clothing--}}
                                                    {{--<span class="TonyM__label TonyM__label--hot">HOT</span>--}}
                                                {{--</span>--}}
                                                                {{--</a>--}}
                                                            {{--</li>--}}
                                                            {{--<li><a href="#">Ankle</a></li>--}}
                                                            {{--<li><a href="#">Belt</a></li>--}}
                                                        {{--</ul>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="TonyM__ttl TonyM--gr12-c3">--}}
                                                        {{--<a href="#">--}}
                                                            {{--<span class="TonyM__ttl-img">--}}
                                                                {{--Power Banks--}}
                                                                {{--<span><img src="images/megamenu/collections/megamenu-collection-04.jpg" alt="Image name"></span>--}}
                                                            {{--</span>--}}
                                                        {{--</a>--}}
                                                        {{--<ul>--}}
                                                            {{--<li><a href="#">USB Port(s)</a></li>--}}
                                                            {{--<li><a href="#">USB Device Charging</a></li>--}}
                                                            {{--<li><a href="#">Overload Protection</a></li>--}}
                                                            {{--<li><a href="#">Rechargeable</a></li>--}}
                                                            {{--<li><a href="#">Wireless</a></li>--}}
                                                        {{--</ul>--}}
                                                    {{--</li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="TonyM__bx-r TonyM--gr6-c2">--}}
                                            {{--<div class="TonyM__prm ttg-image-scale">--}}
                                                {{--<img src="images/megamenu/megamenu-01.jpg" alt="Image name">--}}
                                                {{--<div class="TonyM__prm-text TonyM__prm-text--dsgn-white">--}}
                                                    {{--<span class="colorize-theme2-c">Headphones</span>--}}
                                                    {{--<p class="colorize-theme2-c">Superior sound quality</p>--}}
                                                    {{--<a href="#" class="btn">Shop now!</a>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</li>--}}

                        </ul>
                    </nav>
                </div>
                <div class="tt-header__sidebar">
                    <div class="tt-header__options">
                        <a href="#" class="tt-header__btn tt-header__btn-menu"><i class="icon-menu"></i></a>
                        <div role="search" class="tt-header__search">
                            <form action="#" class="tt-header__search-form">
                                <input type="search" name="q" class="form-control" placeholder="Search...">
                            </form>
                            <div class="tt-header__search-dropdown"></div>
                            <a href="#" class="tt-header__btn tt-header__btn-open-search"><i class="icon-search"></i></a>
                            <a href="#" class="tt-header__btn tt-header__btn-close-search"><i class="icon-cancel-1"></i></a>
                        </div>
                        <div>
                            <a href="#" class="tt-header__btn tt-header__btn-user"><i class="icon-user-outline"></i></a>
                            <div class="tt-header__user">
                                <ul class="tt-list-toggle">
                                    <li><a href="my-account">My account</a></li>
                                    <li><a href="checkout-1">Checkout</a></li>
                                    <li><a href="login">Login</a></li>
                                    <li><a href="register">Register</a></li>

                                    <div class="tt-header__login">
                                        <h6>Login</h6>
                                        <form action="#">
                                            <input type="email" class="form-control" placeholder="Email" required="required">
                                            <input type="password" class="form-control" placeholder="Password" required="required">
                                            <button type="submit" class="btn">Login</button>
                                            <div>
                                                <a href="account">Forget your password?</a>
                                            </div>
                                        </form>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        {{--<a href="wishlist" class="tt-header__btn tt-header__btn-wishlist">--}}
                            {{--<i class="icon-heart-empty-2"></i>--}}
                            {{--<span>2</span>--}}
                        {{--</a>--}}
                        <div>
                            <a href="cart" class="tt-header__btn tt-header__btn-cart">
                                <i class="icon-shop24"></i>
                                <span>{{count( $cartItems)}}</span>
                            </a>

                                    <div class="tt-header__cart">

                                        <div class="tt-header__cart-content">

                                            <div class="tt-summary__products tt-summary--border">

                                                <div class="tt-header__cart-content">
                                                    <h6 class="text-center">There is {{count($cartItems)}} item in your bag</h6>

                                                    <ul class="colorize-bd">
                                                    <?php $sum=0;?>
                                                    @foreach($carts as $cart)
                                                        @foreach($cartItems as $product)
                                                            @if($cart->product_id == $product->id  )

                                                                <li>
                                                                    <?php
                                                                    $total=$product->price*$cart->qty;
                                                                    $sum=$sum+$total;
                                                                    ?>

                                                                    <div>
                                                                        <a href="{{ route('product.detail',$product->id) }}"><img src="{{asset('uploads/images/'.$product->image)}}"
                                                                                                                                  alt="Image name"></a>

                                                                    </div>

                                                                    <div>

                                                                        <p>{{$product->description}}</p>
                                                                            <span class="tt-summary__products_price">
                                                                        <span class="tt-summary__products_price-count">{{ $cart->qty }}</span>
                                                                        <span>x</span>
                                                                        <span class="tt-summary__products_price-val">
                                                                        <span class="tt-price">
                                                                        <span>{{ $product->price }}</span>
                                                                        </span>
                                                                          </span>
                                                                              </span>

                                                                    </div>
                                                                        <div>
                                                                            <form action="{{ route('cart.destroy',$cart->id) }}" method="post" style="display: inline">
                                                                                {{ csrf_field() }}
                                                                                {{ method_field('DELETE') }}
                                                                                <button type="submit" class="tt-header__cart-checkout btn colorize-btn2" onclick="return confirm('Are You Sure you Want To Remove product ?')"><span class="glyphicon glyphicon-trash"></span></button>
                                                                            </form>
                                                                            <div class="tt-counter tt-counter__inner" data-min="1" data-max="10">
                                                                                <form action="{{url( '/cart/'.$cart->id) }}" method="POST" >
                                                                                    {{ csrf_field() }}
                                                                                    {{method_field('PUT')}}
                                                                                    <button type="submit" class="tt-header__cart-checkout btn colorize-btn2"><span class="glyphicon glyphicon-edit"></span></button>
                                                                                    <input type="hidden" name="session_id" class="form-control" value="{{$id}}">
                                                                                    <input type="hidden" class="form-control" value="{{ $product->price }}" name="price">
                                                                                    <input type="number" class="form-control" value="{{ $cart->qty }}" name="qty" >

                                                                                </form>

                                                                            </div>
                                                                        </div>
                                                                </li>
                                                            @endif

                                                        @endforeach
                                                    @endforeach

                                                </ul>
                                                    </div>


                                            </div>
                                            <script>
                                                require(['app'], function () {
                                                    require(['modules/toggleProductParam']);
                                                });
                                            </script>
                                            <div class="tt-summary--border">

                                                <div class="tt-summary__total">
                                                    <p>Grand Total: BDT:{{ number_format($sum,2,'.',',').'/-' }}</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <a href="{{route('cart.index')}}" class="tt-header__cart-viewcart btn"><i class="icon-shop24"></i> View Cart</a>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <a href="{{route("checkout.create")}}" class="tt-header__cart-checkout btn colorize-btn2"><i class="icon-check"></i> Checkout</a>
                                                    </div>
                                                </div>
                                            </div>

                                </div>

                            </div>

                        </div>
                        <div>
                            <a href="#" class="tt-header__btn tt-header__btn-settings"><i class="icon-cog"></i></a>
                            <div class="tt-header__settings">
                                <ul class="tt-list-toggle">
                                    <li class="tt-list-toggle__open"><a href="#">language: USA</a>
                                        <ul>
                                            <li><a href="#">POL</a></li>
                                            <li class="active"><a href="#">USA</a></li>
                                            <li><a href="#">RUS</a></li>
                                        </ul>
                                    </li>
                                    <li class="tt-list-toggle__open"><a href="#">Currency: <span>USD</span></a>
                                        <ul id="currencies">
                                            <li class="active"><a href="#">USD</a></li>
                                            <li><a href="#">RUB</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- MAIN -->
@yield('main_content')

<!-- FOOTER -->
<footer>
    <div class="tt-footer tt-footer__01 ">
        <div class="tt-footer__content">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <a href="{{ route('home') }}" class="tt-logo">
                            <img src="{{ asset('images/ecomerce.png') }}" alt="Image name">
                        </a>
                    </div>
                    <div class="col-lg-6">
                        <div class="tt-footer__list-menu">
                            <div class="row">
                                @foreach($pages as $page)
                                <div class="col-sm-6">
                                    {{--@foreach($pages as $page)--}}
                                       <a href="{{ route('page.detail',$page->id) }}"> {{ $page->page_title }}</a><br/>
                                        {{--@endforeach--}}
                              </div>
                                @endforeach


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <span class="tt-footer__title">Newsletter Signup</span>
                        <div class="tt-footer__newsletter">
                            <p>Sign up for our e-mail and be the first who know our special offers! Furthermore, we will
                                give a <span>15% discount</span> on the next order after you sign up.</p>
                            <form class="form-horizontal" method="POST" action="{{ route('subscriber.store') }}">
                                {{ csrf_field() }}
                                <table>
                                    <tr><td>
                                <input type="text" name="mail" class="form-control"
                                       placeholder="Enter please your e-mail">
                                        </td>
                                    <td>
                                <button type="submit" class="btn">
                                    <i class="tt-newsletter__text-wait"></i>
                                    <span class="tt-newsletter__text-default">Subscribe!</span>
                                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                                </button>
                                    </td></tr>
                                    </table>
                            </form>
                        </div>
                        <div class="tt-footer__social">
                            <div class="tt-social-icons tt-social-icons--style-01">
                                <a href="https://www.facebook.com/nighat.parvin.50" class="tt-btn">
                                    <i class="icon-facebook"></i>
                                </a>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <span class="tt-footer__copyright">&copy; {{ Carbon\carbon::now()->year }} . All Rights Reserved.</span>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="tt-footer__to-top tt-footer__to-top-desktop">
            <i class="icon-up-open-1"></i>
            <i class="icon-up"></i><span>Top</span>
        </a>
    </div>
</footer>

<!-- JAVA SCRIPT -->
<!--plugins-->
<script src="{{ asset('vendor/scrollSmooth/SmoothScroll.js') }}"></script>
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/cookie/jquery.cookie.js') }}"></script>
<script src="{{ asset('vendor/jquery/ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('vendor/velocity/velocity.min.js') }}"></script>
<script src="{{ asset('vendor/modernizr/modernizr.js') }}"></script>
<script src="{{ asset('vendor/lazyLoad/jquery.lazy.min.js') }}"></script>
<script src="{{ asset('vendor/lazyLoad/jquery.lazy.plugins.min.js') }}"></script>
<script src="{{ asset('vendor/tonymenu/js/tonymenu.js') }}"></script>
<script src="{{ asset('vendor/perfectScrollbar/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('vendor/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('vendor/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('vendor/countdown/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('vendor/moment/moment.js') }}"></script>
<script src="{{ asset('vendor/moment/moment-timezone.js') }}"></script>
<script src="{{ asset('vendor/slick/slick.min.js') }}"></script>
<script src="{{ asset('vendor/magnificPopup/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('vendor/elevateZoom/jquery.elevateZoom-3.0.8.min.js') }}"></script>
<script src="{{ asset('vendor/stickyBlock/sticky-sidebar.min.js') }}"></script>
<script src="{{ asset('vendor/rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('vendor/instafeed/instafeed.min.js') }}"></script>
<script src="{{ asset('vendor/jquery/jquery-bridget.js') }}"></script>
<script src="{{ asset('vendor/imagesLoaded/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('vendor/masonry/masonry.pkgd.min.js') }}"></script>
<script src="{{ asset('vendor/swiper/js/swiper.min.js') }}"></script>
<script src="{{ asset('vendor/fotorama/fotorama.js') }}"></script>
@stack('scripts')

<!--modules-->
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
