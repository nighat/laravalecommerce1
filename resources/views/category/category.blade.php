@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">

                    <div class="panel-heading"><h5 style="text-align: center">Category Create Form</h5></div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('category.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image" class="col-md-4 control-label">Image</label>

                                <div class="col-md-6">
                                    <input id="image" type="file" class="form-control" name="image" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="created_by" class="col-md-4 control-label">Created By</label>

                                <div class="col-md-6">
                                    <input id="created_by" type="text" class="form-control" name="created_by" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="updated_by" class="col-md-4 control-label">Updated By</label>

                                <div class="col-md-6">
                                    <input id="updated_by" type="text" class="form-control" name="updated_by" >
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('category.index') }}" class=" btn btn-danger">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>
@endsection