

@extends('layouts/mogo')

@section('main_content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">


                    <div class="panel-heading ">
                        <h5 style="text-align: center">Category Details
                            <a href="{{ route('category.create') }}" class=" btn btn-danger pull-right">Create</a>
                            <a href="{{ route('category.index') }}" class=" btn btn-danger pull-right">Category</a>
                        </h5>

                    </div>

                    <div>
                          <img style="height: 400px; width: 600px" src="{{asset('uploads/category/'.$category->image)}}">

                    </div>
                   <div>
                        <b>Title:-</b>  {{ $category->title}}
                    </div>
                    <div>
                        <b>Created By:-</b>    {{ $category->created_by}}
                    </div>
                    <div>
                        <b>Updated By:-</b>    {{ $category->updated_by}}
                    </div>
                </div>

        </div>
    </div>
@endsection