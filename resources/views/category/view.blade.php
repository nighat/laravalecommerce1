

@extends('layouts/mogo')

@section('main_content')


    <main>

        <div class="tt-layout tt-sticky-block__parent ">
            <div class="tt-layout__content">
                <div class="container">
                    <div class="tt-listing-page">
                        @include('include/breadcrumbs')
                        <div class="tt-listing-page__category-name"><h3>Shop Now Category</h3></div>
                        <div class="tt-listing-page__view-options tt-vw-opt">
                            <div class="row">
                                <div class="col-xl-6 col-lg-5 col-md-5 col-xs-12">
                                    <div class="tt-vw-opt__sort">
                                        <span>Sort:</span>
                                        <label class="tt-select">
                                            <select class="form-control">
                                                <option>Default</option>
                                                <option>Default #2</option>
                                                <option>Default #3</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="tt-vw-opt__direction">
                                        <a href="#" class="active"><i class="icon-down"></i></a>
                                        <a href="#"><i class="icon-up"></i></a>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-5 col-xs-8">
                                    <div class="tt-vw-opt__info">
                                        <span>Showing 1–9 of 23 results</span>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-2 col-xs-4">
                                    <div class="tt-vw-opt__length">
                                        <span>Show:</span>
                                        <label class="tt-select">
                                            <select class="form-control">
                                                <option>12</option>
                                                <option>9</option>
                                                <option>6</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="tt-vw-opt__grid">
                                        <div class="tt-product-btn-vw" data-control=".tt-product-view">
                                            <label>
                                                <input type="radio" name="product-btn-vw" checked>
                                                <i class="icon-th-large"></i>
                                                <i class="icon-check-empty"></i>
                                            </label>
                                            <label>
                                                <input type="radio" name="product-btn-vw"
                                                       data-view-class="tt-product-view--list">
                                                <i class="icon-th"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tt-listing-page__products tt-layout__mobile-full">

                            <div class="tt-product-view row">
                                @foreach( $categories as $category )
                                <div class="col-sm-6 col-xl-4 col-xxl-3">

                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">

                                        <div class="tt-product__image">

                                                <img style="height: 400px; width: 500px" src="{{asset('uploads/category/'.$category->image)}}" data-srcset= "{{asset('uploads/category/'.$category->image)}}"
                                                     data-retina="{{asset('uploads/category/'.$category->image)}}"
                                                     alt="Elegant and fresh. A most attractive mobile power supply.">

                                            <div class="tt-product__labels">
                                                <span class="tt-label__new">New</span>
                                                <span class="tt-label__hot">Hot</span>

                                                <span class="tt-label__sale">Sale</span>
                                                <span class="tt-label__discount">${{  $category->price}}</span>



                                                <div> @if ($category->qty >=1)
                                                        <span class="tt-label__in-stock">In Stock</span>

                                                    @else
                                                        <span class="tt-label__out-stock">Out Stock</span>
                                                    @endif
                                                   </div>
                                            </div>
                                        </div>


                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                            <div class="tt-product__content">
                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">{{  $category->title}}</a>
                                            </span>
                                                </h3>
                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">{{  $product->description}}</a>
                                            </span>
                                                </p>
                                                <p class="tt-product__description">{{  $product->description}}</p>
                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>${{  $product->price}}</span>

                                                </span>
                                            </span>
                                                </div>


                                                <div class="ttg-text-animation--emersion">
                                                    <div class="tt-product__buttons">

                                                        <form action="{{ route('cart.store') }}" method="POST">
                                                            {{ csrf_field() }}

                                                            <input type="hidden" name="qty" class="form-control" value="1" value="{{$product->qty}} ">
                                                            <input type="hidden" name="product_id" class="form-control" value="{{ $product->id}}">
                                                            <input type="hidden" name="session_id" class="form-control" value="{{ $id }}">
                                                            <input type="hidden" name="price" class="form-control" value="{{ $product->price}}">

                                                            <a href="{{route('cart.index')}}" >
                                                                <button type="submit" class="tt-btn colorize-btn5  tt-btn__state--active">
                                                                    <i class="icon-shop24"></i>
                                                                    <spam></spam>

                                                                </button>
                                                            </a>

                                                            <a href="{{ route('product.detail',$product->id) }}" class="tt-btn colorize-btn4">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </form>

                                                        <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like tt-btn__state--active">
                                                            <i class="icon-heart-empty-2"></i>
                                                        </a>
                                                        <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare tt-btn__state--active">
                                                            <i class="icon-untitled-1"></i>
                                                        </a>
                                                        <a href="{{ route('product.detail',$product->id) }}" class="tt-btn colorize-btn4">
                                                            <i class="icon-eye"></i>
                                                        </a>

                                                    </div>
                                                </div>
                                                <div class="ttg-text-animation--emersion">

                                                    <span  style="color: darkgreen">{{$category->created_at}}</span>

                                                        <div class="tt-product__countdown" data-date=" {{$product->created_at}}" data-zone="Europe/Madrid">

                                                        </div>




                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                @endforeach
                            </div>
                        </div>
                        {{ $category->links() }}
                    </div>
                </div>
            </div>
        </div>

        <div class="tt-add-to-cart" data-active="true">
            <i class="icon-check"></i>
            <p>Added to Cart Successfully!</p>
            <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
        </div>

        <div class="tt-newsletter-popup" data-active="true">
            <div class="tt-newsletter-popup__text-01">
                <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
            </div>
            <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
            <p>By signing up, you accept the terms & Privacy Policy.</p>
            <div class="ttg-mb--30">
                <form action="#" class="tt-newsletter tt-newsletter--style-02">
                    <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                    <button type="submit" class="btn">
                        <i class="tt-newsletter__text-wait"></i>
                        <span class="tt-newsletter__text-default">Subscribe!</span>
                        <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                        <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                    </button>
                </form>
            </div>
            <div class="tt-newsletter-popup__social">
                <div class="tt-social-icons tt-social-icons--style-03">
                    <a href="#" class="tt-btn">
                        <i class="icon-facebook"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-twitter"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-gplus"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-instagram-1"></i>
                    </a>
                    <a href="#" class="tt-btn">
                        <i class="icon-youtube-play"></i>
                    </a>
                </div>
            </div>
            <label class="tt-newsletter-popup__show_popup tt-checkbox">
                <input type="checkbox" name="show-nawslatter">
                <span></span>
                Don't show this popup again
            </label>
        </div>
    </main>




@endsection