

@extends('layouts/mogo')

@section('main_content')
       <div class="container">
              <div class="row">
                     <div class="col-md-10 col-md-offset-2">


                                   <div class="panel-heading ">
                                          <h5 style="text-align: center">Picture Details
                                                 <a href="{{ route('picture.create') }}" class=" btn btn-danger pull-right">Create</a>
                                                 <a href="{{ route('picture.index') }}" class=" btn btn-danger pull-right">Picture</a>
                                          </h5>

                                   </div>

                                   <div> <b>Path:-</b>   {{ $picture->path}} </div>
                                   <div> <b>Type:-</b>   {{ $picture->type}} </div>
                                   <div> <b>Product Id:-</b>  {{ $picture->product_id}}</div>
                                   <div> <b>Create By:-</b> {{ $picture->created_by}}</div>
                                  <div> <b>Update By:-</b>  {{ $picture->updated_by}}</div>


                   </div>

              </div>
       </div>





@endsection