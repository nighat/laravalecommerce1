@extends('layouts/mogo')

@section('main_content')
    <div class="panel-heading">
        <h5 style="text-align: center">Picture Lists <a href="{{ route('picture.create') }}" class=" btn btn-danger pull-right">Add New Picture</a></h5>
        <div style="background:#00b38f; color: #ffffff; width: 600px;text-align: center; font-size: 20px;">{{ session('message') }}</div>



    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Path</th>
            <th>Type</th>
            <th>Product Id</th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach($pictures as $picture)

            <tr>
                <td>{{ $loop->index + 1}}</td>


                <td>{{ $picture->path }}</td>
                <td>{{ $picture->type }}</td>
                <td>{{ $picture->product_id }}</td>
                <td>{{ $picture->created_by}}</td>
                <td>{{ $picture->updated_by}}</td>

                <td> <a href="{{ route('picture.show',$picture->id) }}" class=" btn btn-danger"><span class=" glyphicon glyphicon-eye-open"></span></a>
                    <a href="{{ route('picture.edit',$picture->id) }}" class=" btn btn-info"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $picture->id }}" method="POST" action="{{ route('picture.destroy',$picture->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $picture->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-info"><span class="glyphicon glyphicon-trash"></span></a>

                </td>


            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th>S.No</th>
            <th>Path</th>
            <th>Type</th>
            <th>Product Id</th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Action</th>


        </tr>
        </tfoot>
    </table>

    {{ $pictures->links() }}


@endsection