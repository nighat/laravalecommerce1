@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                    <div class="panel-heading"><h3 style="text-align: center">Picture Create Form</h3>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('picture.store') }}">
                            {{ csrf_field() }}


                             <div class="form-group">
                                <label for="path" class="col-md-4 control-label">Path</label>

                                <div class="col-md-6">
                                    <input id="path" type="text" class="form-control" name="path" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="type" class="col-md-4 control-label">Type</label>

                                <div class="col-md-6">
                                    <input id="type" type="text" class="form-control" name="type" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="product_id" class="col-md-4 control-label">Product Id</label>

                                <div class="col-md-6">
                                    <input id="product_id" type="text" class="form-control" name="product_id" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="created_by" class="col-md-4 control-label">Created By</label>

                                <div class="col-md-6">
                                    <input id="created_by" type="text" class="form-control" name="created_by" >
                                </div>
                            </div>

                             <div class="form-group">
                                <label for="updated_by" class="col-md-4 control-label">Updated By</label>

                                <div class="col-md-6">
                                    <input id="updated_by" type="text" class="form-control" name="updated_by" >
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('picture.index') }}" class=" btn btn-danger">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>

            </div>
        </div>
    </div>
@endsection