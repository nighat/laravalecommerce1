
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="tt-layout tt-sticky-block__parent ">
    <div class="tt-layout__content">
        <div class="container">
            {{--@include('include/breadcrumbs')--}}

            <div class="tt-page__name text-center">
                <h2 style=";color: #1b6d85">
                    Order Products

                </h2>
                <p class="text-center">your order id  is!  # @if(session('order_id'))
                        <a href="">{{session('order_id')}}</a>

                    @endif
                </p>
            </div>
        </div>
<table class="table table-condensed">
    <tr style="color: #1b6d85">
        <th>Product Name</th>
        <th>Picture</th>
        <th>Qty</th>
        <th>price</th>
        <th>sku</th>
        <th>meta keyword</th>
        <th>total</th>
    </tr>
    <?php $subtotal=0;?>
    @foreach ($orderItems as $orderItem)
        @foreach ($productItems as $productItem)
            <tr>
                @if($orderItem->product_id == $productItem->id)
                    <?php
                    $total= $orderItem->price*$orderItem->qty;
                    $subtotal+=$total;
                    ?>
                    <td>{{$productItem->title}}</td>
                    <td> <img src="{{URL('/uploads/images/'.$productItem->image)}}" style="width:130px;height:70px"></td>
                    <td>{{$orderItem->qty}}</td>
                    <td>{{number_format($orderItem->price,2,'.',',').'/-'}}</td>
                    <td>{{$productItem->sku}}</td>
                    <td>{{$productItem->meta_keyword}}</td>
                    <td>{{number_format($total ,2,'.',',').'/-'}}</td>
                @endif
            </tr>
        @endforeach
    @endforeach
    <tr>
        <td colspan="5"></td>
        <td>SubTotal</td>
        <td>{{number_format($subtotal,2,'.',',').'/-'}}</td>
    </tr>
</table>
    </div>

    <b>Thank you for your purchase!</b>
</div>
</div>
</body>
</html>


