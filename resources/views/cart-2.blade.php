@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>Shopping Cart</h1>
                </div>

                <div class="tt-cart">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="tt-cart__caption">
                                <div class="row">
                                    <div class="col-md-6"><span>Products</span></div>
                                    <div class="col-md-2 text-center"><span>Price</span></div>
                                    <div class="col-md-2 text-center"><span>Quantity</span></div>
                                    <div class="col-md-2 text-center"><span>Total</span></div>
                                </div>
                            </div>
                            <div class="tt-cart__list">
                                <div class="tt-cart__product">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="#" class="tt-cart__product_del"><i class="icon-trash"></i></a>
                                            <a href="#" class="tt-cart__product_image"><img
                                                        src="images/cart/cart-01.jpg" alt="Image name"></a>
                                            <div class="tt-cart__product_info">
                                                <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                                                <p>Color: <span>Orange</span></p>
                                                <p>Size: <span>XL</span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-4 text-center">
                                            <div class="tt-cart__product_price">
                                                <div class="tt-price">
                                                    <span>$25</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-4 text-center">
                                            <div class="tt-counter tt-counter__inner" data-min="1" data-max="10">
                                                <input type="text" class="form-control" value="1">
                                                <div class="tt-counter__control">
                                                    <span class="icon-up-circle" data-direction="next"></span>
                                                    <span class="icon-down-circle" data-direction="prev"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-4 text-center">
                                            <div class="tt-cart__product_price">
                                                <div class="tt-price">
                                                    <span>$28</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tt-cart__product">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="#" class="tt-cart__product_del"><i class="icon-trash"></i></a>
                                            <a href="#" class="tt-cart__product_image"><img
                                                        src="images/cart/cart-01.jpg" alt="Image name"></a>
                                            <div class="tt-cart__product_info">
                                                <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                                                <p>Color: <span>Orange</span></p>
                                                <p>Size: <span>XL</span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-4 text-center">
                                            <div class="tt-cart__product_price">
                                                <div class="tt-price tt-price--sale">
                                                    <span>$22</span>
                                                    <span>$28</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-4 text-center">
                                            <div class="tt-counter tt-counter__inner" data-min="1" data-max="10">
                                                <input type="text" class="form-control" value="1">
                                                <div class="tt-counter__control">
                                                    <span class="icon-up-circle" data-direction="next"></span>
                                                    <span class="icon-down-circle" data-direction="prev"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-4 text-center">
                                            <div class="tt-cart__product_price">
                                                <div class="tt-price">
                                                    <span>$24</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tt-cart__product">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="#" class="tt-cart__product_del"><i class="icon-trash"></i></a>
                                            <a href="#" class="tt-cart__product_image"><img
                                                        src="images/cart/cart-01.jpg" alt="Image name"></a>
                                            <div class="tt-cart__product_info">
                                                <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                                                <p>Color: <span>Orange</span></p>
                                                <p>Size: <span>XL</span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-4 text-center">
                                            <div class="tt-cart__product_price">
                                                <div class="tt-price">
                                                    <span>$24</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-4 text-center">
                                            <div class="tt-counter tt-counter__inner" data-min="1" data-max="10">
                                                <input type="text" class="form-control" value="1">
                                                <div class="tt-counter__control">
                                                    <span class="icon-up-circle" data-direction="next"></span>
                                                    <span class="icon-down-circle" data-direction="prev"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-4 text-center">
                                            <div class="tt-cart__product_price">
                                                <div class="tt-price">
                                                    <span>$34</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tt-cart__footer">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Coupone code:</p>
                                        <form action="#">
                                            <input type="text" class="form-control" placeholder="Code">
                                            <button type="submit" class="btn">Apply</button>
                                        </form>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a href="#" class="btn btn-type--icon colorize-btn6"><i class="icon-arrows-cw-1"></i><span>Update Cart</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="tt-summary">
                                <div class="tt-summary--border">
                                    <h4>Cart Total</h4>
                                </div>
                                <div class="tt-summary--border">
                                    <div class="tt-summary__total ttg-mb--20">
                                        <p>Subtotal: <span>$78</span></p>
                                    </div>
                                    <div class="row">
                                        <div class="col-xxl-3 col-xl-4 col-lg-12 col-md-2 col-sm-3">
                                            <h5 class="ttg-mb--10">Shipping:</h5>
                                        </div>
                                        <div class="col-xxl-9 col-xl-8 col-lg-12 col-md-10 col-sm-9">
                                            <ul class="tt-summary__categories tt-categories tt-categories__toggle ttg-mb--10">
                                                <li><a href="#">Flat Rate: $12</a></li>
                                                <li><a href="#">Free Shipping</a></li>
                                                <li><a href="#">Flat Rate: $60</a></li>
                                                <li><a href="#">Local Delivery: $5</a></li>
                                            </ul>
                                            <p class="ttg-f--16 ttg-fw--bold">Calulate Shipping</p>
                                            <div class="tt-summary__form">
                                                <form action="#" class="ttg-mt--20">
                                                    <div class="tt-select">
                                                        <select class="form-control">
                                                            <option>Country</option>
                                                            <option>Country #2</option>
                                                            <option>Country #3</option>
                                                        </select>
                                                    </div>
                                                    <div class="tt-select">
                                                        <select class="form-control">
                                                            <option>Region</option>
                                                            <option>Region #2</option>
                                                            <option>Region #3</option>
                                                        </select>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="Postcode/ZIP">
                                                    <a href="#" class="btn btn-type--icon"><i
                                                                class="icon-arrows-cw-1"></i><span>Update Total</span></a>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tt-summary__total tt-summary__total--m-price-50">
                                    <p>Total: <span>$78</span></p>
                                </div>
                                <a href="$" class="tt-summary__btn-checkout btn btn-type--icon colorize-btn6"><i
                                            class="icon-check"></i><span>Proeed to Checkout</span></a>
                            </div>
                            <script>
                                require(['app'], function () {
                                    require(['modules/category']);
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection


