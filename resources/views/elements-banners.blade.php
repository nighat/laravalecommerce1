@extends ('layouts/mogo')


@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                 @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>Banners</h1>
                </div>

                <div class="ttg-mt--60 ttg-mb--70">
                    <div class="container-fluid ttg-cont-padding--none">
                        <div class="row ttg-grid-padding--none">
                            <div class="col-sm-8">
                                <div class="row ttg-grid-padding--none">
                                    <div class="col-sm-6">
                                        <a href="#" class="tt-promobox
                                                           ttg-text-animation-parent
                                                           ttg-image-translate--right
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md"
                                        >
                                            <div class="tt-promobox__content">
                                                <img src="#" data-srcset="images/promoboxes/promobox-01.jpg"
                                                     alt="Image name">
                                                <div class="tt-promobox__text"
                                                     data-resp-md="md"
                                                     data-resp-sm="md"
                                                     data-resp-xs="sm">
                                                    <div>Watches</div>
                                                </div>
                                                <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                    <div class="tt-promobox__hover-bg"></div>
                                                    <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                        <div class="ttg-text-animation--emersion">
                                                            <span>Watches</span>
                                                        </div>
                                                        <p class="ttg-text-animation--emersion">
                                                            <span><span>28</span> products</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="#" class="tt-promobox
                                                           ttg-text-animation-parent
                                                           ttg-image-translate--top
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                            <div class="tt-promobox__content">
                                                <img src="#" data-srcset="images/promoboxes/promobox-02.jpg"
                                                     alt="Image name">
                                                <div class="tt-promobox__text"
                                                     data-resp-md="md"
                                                     data-resp-sm="md"
                                                     data-resp-xs="sm">
                                                    <div class="colorize-theme2-c">Trackers</div>
                                                </div>
                                                <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                    <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                                    <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                        <div class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c">Trackers</span>
                                                        </div>
                                                        <p class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c"><span>46</span> products</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-12">
                                        <a href="#" class="tt-promobox
                                                           tt-promobox__size-wide
                                                           ttg-text-animation-parent
                                                           ttg-image-scale
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                            <div class="tt-promobox__content">
                                                <img src="#" data-srcset="images/promoboxes/promobox-03.jpg"
                                                     alt="Image name">
                                                <div class="tt-promobox__text"
                                                     data-resp-md="md"
                                                     data-resp-sm="md"
                                                     data-resp-xs="sm">
                                                    <div class="colorize-theme2-c">Headphones</div>
                                                </div>
                                                <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                    <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                                    <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                        <div class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c">Headphones</span>
                                                        </div>
                                                        <p class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c">32 products</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" class="tt-promobox
                                                   tt-promobox__size-high
                                                   ttg-text-animation-parent
                                                   ttg-image-translate--left
                                                   ttg-animation-disable--md
                                                   tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img src="#" data-srcset="images/promoboxes/promobox-04.jpg" alt="Image name">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="md"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Earphones</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Earphones</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">24 products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ttg-mb--70">
                    <div class="container">
                        <div class="row ttg-grid-pdg-btm--sm">
                            <div class="col-md-4">
                                <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                                                                                                     ttg-text-animation-parent
                                                                                                     ttg-image-translate--left
                                                                                                     ttg-animation-disable--md
                                                                                                     tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-11.jpg" alt="Image name">
                                        <div class="tt-promobox__text "
                                             data-resp-md="md"
                                             data-resp-sm="lg"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Trackers</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Trackers</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c"><span class="colorize-theme-c">12</span> products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                                                                                                       ttg-text-animation-parent
                                                                                                       ttg-image-scale
                                                                                                       ttg-animation-disable--md
                                                                                                       tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-12.jpg" alt="Image name">
                                        <div class="tt-promobox__text "
                                             data-resp-md="md"
                                             data-resp-sm="lg"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Watches</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Watches</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c"><span class="colorize-theme-c">28</span> products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                                                                                                       ttg-text-animation-parent
                                                                                                       ttg-image-translate--top
                                                                                                       ttg-animation-disable--md
                                                                                                       tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-13.jpg" alt="Image name">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="lg"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Audio</div>
                                        </div>
                                        <div class="tt-promobox__hovertt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Audio</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c"><span class="colorize-theme-c">8</span> products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ttg-mb--70">
                    <div class="container">
                        <div class="row ttg-grid-pdg-btm--sm">
                            <div class="col-md-4 col-lg-12 col-xl-4">
                                <div class="tt-promobox
                                            ttg-image-translate--top
                                            ttg-animation-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-14.jpg" alt="Image name">
                                        <div class="tt-promobox__hover tt-promobox__hover--up">
                                            <div class="tt-promobox__hover-bg
                                                        tt-promobox__hover-bg--visible
                                                        ttg-bg--theme">
                                            </div>
                                            <div class="tt-promobox__text
                                                        tt-promobox__point-lg--top
                                                        tt-promobox__point-lg--left">
                                                <a href="listing-with-custom-html-block.html" class="colorize-theme2-c">Audio</a>
                                                <ul>
                                                    <li><a href="#">Wireless</a></li>
                                                    <li><a href="#">Built-In Microphone</a></li>
                                                    <li><a href="#">Bluetooth Enabled</a></li>
                                                    <li><a href="#">Rechargeable</a></li>
                                                    <li><a href="#">USB Device Charging</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-12 col-xl-4">
                                <div class="tt-promobox
                                            ttg-image-translate--right
                                            ttg-animation-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-15.jpg" alt="Image name">
                                        <div class="tt-promobox__hover tt-promobox__hover--up">
                                            <div class="tt-promobox__hover-bg
                                                        tt-promobox__hover-bg--visible
                                                        ttg-bg--theme">
                                            </div>
                                            <div class="tt-promobox__text
                                                        tt-promobox__point-lg--top
                                                        tt-promobox__point-lg--left">
                                                <a href="listing-with-custom-html-block.html" class="colorize-theme2-c">Watches</a>
                                                <ul>
                                                    <li><a href="#">Wireless</a></li>
                                                    <li><a href="#">Built-In Microphone</a></li>
                                                    <li><a href="#">Bluetooth Enabled</a></li>
                                                    <li><a href="#">Rechargeable</a></li>
                                                    <li><a href="#">USB Device Charging</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-12 col-xl-4">
                                <div class="tt-promobox
                                            ttg-image-translate--bottom
                                            ttg-animation-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-16.jpg" alt="Image name">
                                        <div class="tt-promobox__hover tt-promobox__hover--up">
                                            <div class="tt-promobox__hover-bg
                                                        tt-promobox__hover-bg--visible
                                                        ttg-bg--theme">
                                            </div>
                                            <div class="tt-promobox__text
                                                        tt-promobox__point-lg--top
                                                        tt-promobox__point-lg--left">
                                                <a href="listing-with-custom-html-block.html" class="colorize-theme2-c">Trackers</a>
                                                <ul>
                                                    <li><a href="#">Wireless</a></li>
                                                    <li><a href="#">Built-In Microphone</a></li>
                                                    <li><a href="#">Bluetooth Enabled</a></li>
                                                    <li><a href="#">Rechargeable</a></li>
                                                    <li><a href="#">USB Device Charging</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ttg-mb--70">
                    <div class="container-fluid ttg-cont-padding--none">
                        <div class="row ttg-grid-padding--none">
                            <div class="col-sm-6 col-lg-4">
                                <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                                                                                                       ttg-text-animation-parent
                                                                                                       ttg-image-translate--right
                                                                                                       ttg-animation-disable--md
                                                                                                       tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-21.jpg" alt="Image name">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="lg"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Trackers</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Trackers</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c"><span>28</span> products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                                                                                                       ttg-text-animation-parent
                                                                                                       ttg-image-scale
                                                                                                       ttg-animation-disable--md
                                                                                                       tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-22.jpg" alt="Image name">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="lg"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Earphones</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Earphones</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c"><span>28</span> products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                                                                                                       ttg-text-animation-parent
                                                                                                       ttg-image-translate--bottom
                                                                                                       ttg-animation-disable--md
                                                                                                       tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-23.jpg" alt="Image name">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="lg"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Watches</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Watches</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c"><span>28</span> products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                                                                                                       ttg-text-animation-parent
                                                                                                       ttg-image-scale
                                                                                                       ttg-animation-disable--md
                                                                                                       tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-24.jpg" alt="Image name">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="lg"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Headphones</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Headphones</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c"><span>28</span> products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                                                                                                       ttg-text-animation-parent
                                                                                                       ttg-image-translate--top
                                                                                                       ttg-animation-disable--md
                                                                                                       tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-25.jpg" alt="Image name">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="lg"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Speakers</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Speakers</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c"><span>28</span> products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                                                                                                       ttg-text-animation-parent
                                                                                                       ttg-image-translate--left
                                                                                                       ttg-animation-disable--md
                                                                                                       tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-26.jpg" alt="Image name">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="lg"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Power Banks</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Power Banks</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c"><span>28</span> products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ttg-mb--70">
                    <div class="container-fluid ttg-cont-padding--none">
                        <div class="row ttg-grid-padding--none">
                            <div class="col-sm-6 col-lg-4">
                                <a href="listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-image-translate--right
                   ttg-animation-disable--md
                   tt-promobox__hover-disable">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-27.jpg" alt="Image name">
                                        <div class="tt-promobox__text tt-promobox__text--sz-01"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <p class="colorize-theme2-c">Smart Watches</p>
                                            <div class="colorize-theme2-c">$378</div>
                                        </div>
                                        <div class="tt-promobox__text tt-promobox__point-lg--right tt-promobox__point-lg--bottom"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <div class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                                <i class="icon-shop24"></i>
                                            </div>
                                        </div>
                                    </div>
                                </a></div>
                            <div class="col-sm-6 col-lg-4">
                                <a href="listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-image-scale
                   ttg-animation-disable--md
                   tt-promobox__hover-disable">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-28.jpg" alt="Image name">
                                        <div class="tt-promobox__text tt-promobox__text--sz-01"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <p class="colorize-theme2-c">Speaker Bottle</p>
                                            <div class="colorize-theme2-c">$72</div>
                                        </div>
                                        <div class="tt-promobox__text tt-promobox__point-lg--right tt-promobox__point-lg--bottom"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <div class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                                <i class="icon-shop24"></i>
                                            </div>
                                        </div>
                                    </div>
                                </a></div>
                            <div class="col-sm-6 col-lg-4">
                                <a href="listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-image-translate--bottom
                   ttg-animation-disable--md
                   tt-promobox__hover-disable">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-29.jpg" alt="Image name">
                                        <div class="tt-promobox__text tt-promobox__text--sz-01"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <p class="colorize-theme2-c">Monitors Speaker</p>
                                            <div class="colorize-theme2-c">$245</div>
                                        </div>
                                        <div class="tt-promobox__text tt-promobox__point-lg--right tt-promobox__point-lg--bottom"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <div class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                                <i class="icon-shop24"></i>
                                            </div>
                                        </div>
                                    </div>
                                </a></div>
                            <div class="col-sm-6 col-lg-4">
                                <a href="listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-image-scale
                   ttg-animation-disable--md
                   tt-promobox__hover-disable">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-30.jpg" alt="Image name">
                                        <div class="tt-promobox__text tt-promobox__text--sz-01"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <p class="colorize-theme2-c">Headphone</p>
                                            <div class="colorize-theme2-c">$65</div>
                                        </div>
                                        <div class="tt-promobox__text tt-promobox__point-lg--right tt-promobox__point-lg--bottom"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <div class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                                <i class="icon-shop24"></i>
                                            </div>
                                        </div>
                                    </div>
                                </a></div>
                            <div class="col-sm-6 col-lg-4">
                                <a href="listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-image-translate--top
                   ttg-animation-disable--md
                   tt-promobox__hover-disable">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-31.jpg" alt="Image name">
                                        <div class="tt-promobox__text tt-promobox__text--sz-01"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <p class="colorize-theme2-c">Earphone</p>
                                            <div class="colorize-theme2-c">$15</div>
                                        </div>
                                        <div class="tt-promobox__text tt-promobox__point-lg--right tt-promobox__point-lg--bottom"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <div class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                                <i class="icon-shop24"></i>
                                            </div>
                                        </div>
                                    </div>
                                </a></div>
                            <div class="col-sm-6 col-lg-4">
                                <a href="listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-image-translate--left
                   ttg-animation-disable--md
                   tt-promobox__hover-disable">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-32.jpg" alt="Image name">
                                        <div class="tt-promobox__text tt-promobox__text--sz-01"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <p class="colorize-theme2-c">Power Bank</p>
                                            <div class="colorize-theme2-c">$127</div>
                                        </div>
                                        <div class="tt-promobox__text tt-promobox__point-lg--right tt-promobox__point-lg--bottom"
                                             data-resp-xs="sm"
                                             data-resp-md="sm">
                                            <div class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                                <i class="icon-shop24"></i>
                                            </div>
                                        </div>
                                    </div>
                                </a></div>
                        </div>
                    </div>
                </div>

                <div class="ttg-mb--100">
                    <div class="tt-post-img tt-post-img--layers tt-post-img--curtain ttg-image-scale">
                        <img src="images/blog/single/blog-single-06.jpg" alt="Image name">
                        <div class="tt-post-img__text">
                            <div class="tt-post-img__title tt-post-img__title--long">Lorem Ipsum Dolor Sit Amet Conse
                                Ctetur Aadipisicing
                            </div>
                            <div class="tt-post-img__info ttg-f--16 ttg-fw--bold">Lorem ipsum dolor sit amet conse
                                ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                            </div>
                            <a href="#" class="btn">Shop Now!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection