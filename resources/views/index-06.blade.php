@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                <div class="tt-slider-scroll" data-layout="fullscreen">
                    <div class="tt-slider-scroll__nav">
                        <ul>
                            <li class="active"><a href="#"></a></li>
                            <li><a href="#"></a></li>
                            <li><a href="#"></a></li>
                            <li><a href="#"></a></li>
                            <li><a href="#"></a></li>
                            <li><a href="#"></a></li>
                        </ul>
                        <div class="tt-slider-scroll__nav_arrows">
                            <span><i class="icon-left-open-2"></i></span>
                            <span><i class="icon-right-open-2"></i></span>
                        </div>
                    </div>
                    <div class="tt-slider-scroll__item">
                        <img data-srcset="images/slider/11/slide-01.jpg" alt="Image name">
                        <div class="tt-slider-scroll__text">
                            <div>Earphone</div>
                            <span>$82</span>
                            <p>Superior sound quality</p>
                            <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                <i class="icon-shop24"></i>
                            </a>
                        </div>
                    </div>
                    <div class="tt-slider-scroll__item">
                        <img data-srcset="images/slider/11/slide-02.jpg" alt="Image name">
                        <div class="tt-slider-scroll__text">
                            <div>Headphone</div>
                            <span>$127</span>
                            <p>Superior sound quality</p>
                            <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                <i class="icon-shop24"></i>
                            </a>
                        </div>
                    </div>
                    <div class="tt-slider-scroll__item">
                        <img data-srcset="images/slider/11/slide-03.jpg" alt="Image name">
                        <div class="tt-slider-scroll__text">
                            <div>Headphone</div>
                            <span>$127</span>
                            <p>Superior sound quality</p>
                            <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                <i class="icon-shop24"></i>
                            </a>
                        </div>
                    </div>
                    <div class="tt-slider-scroll__item">
                        <img data-srcset="images/slider/11/slide-04.jpg" alt="Image name">
                        <div class="tt-slider-scroll__text">
                            <div>Headphone</div>
                            <span>$127</span>
                            <p>Superior sound quality</p>
                            <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                <i class="icon-shop24"></i>
                            </a>
                        </div>
                    </div>
                    <div class="tt-slider-scroll__item">
                        <img data-srcset="images/slider/11/slide-05.jpg" alt="Image name">
                        <div class="tt-slider-scroll__text">
                            <div>Headphone</div>
                            <span>$127</span>
                            <p>Superior sound quality</p>
                            <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                <i class="icon-shop24"></i>
                            </a>
                        </div>
                    </div>
                    <div class="tt-slider-scroll__item">
                        <img data-srcset="images/slider/11/slide-06.jpg" alt="Image name">
                        <div class="tt-slider-scroll__text">
                            <div>Headphone</div>
                            <span>$127</span>
                            <p>Superior sound quality</p>
                            <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                <i class="icon-shop24"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection