@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')


                <div class="tt-page__name text-center">
                    <h1>Answers to Your Questions</h1>
                </div>
                <div class="tt-page__name-sm text-center">
                    <h2>Orders</h2>
                </div>

                <div class="tt-page__cont-small">
                    <div class="tt-faq tt-faq--arrow">
                        <i class="icon-right-open"></i>
                        <div class="tt-faq__section">
                            <div class="tt-faq__section_head">
                                <i class="icon-down-open"></i>
                                <h5>How do I place an order?</h5>
                            </div>
                            <p>If you have chosen a favorite sporting goods, then click "Add to cart", and it will
                                automatically be placed in your shopping cart. You can then continue to select products
                                in our online store, adding to your shopping cart as sporting goods, as needed.If you
                                have already completed the selection of sporting goods, then proceed directly to
                                checkout.</p>
                        </div>
                        <div class="tt-faq__section">
                            <div class="tt-faq__section_head">
                                <i class="icon-down-open"></i>
                                <h5>How can I cancel or change my order?</h5>
                            </div>
                            <p>Carefully check the contents of your basket. You can change the number of instances of
                                the product or remove individual items by simply pressing the "Recalculate". Further, if
                                all selected correctly, click the "Checkout". On the next step enter personal
                                information (contacts, name, shipping address). You will then check the entered
                                information and click "Send." In the nearest future our manager will contact with you,
                                to clarify again the data and confirm your order. </p>
                        </div>
                        <div class="tt-faq__section">
                            <div class="tt-faq__section_head">
                                <i class="icon-down-open"></i>
                                <h5>What I have to consider in the process of order? </h5>
                            </div>
                            <p>When ordering, specify the actual phone number and shipping address in your account,
                                otherwise the carrier will not be able to reach you and your order will be canceled.
                                Depending on the size and weight of your goods as well as the delivery address specified
                                in your account, you will be offered the available delivery methods. The term of
                                delivery and the cost of courier delivery is calculated automatically by the system
                                depending on your address, weight and size of your order, and the courier company that
                                delivers to your region.</p>
                        </div>
                        <div class="tt-faq__section">
                            <div class="tt-faq__section_head">
                                <i class="icon-down-open"></i>
                                <h5>What is important in the receipt of an order?</h5>
                            </div>
                            <p>Turn on the phone during the delivery day - so the courier can contact you. Upon receipt
                                of the order before payment is required to check the conformity of the goods in the
                                parcel, and completeness of the goods, the courier is to wait until you will check.
                                Fitting and partial redemption not carried out.</p>
                        </div>
                        <div class="tt-faq__section">
                            <div class="tt-faq__section_head">
                                <i class="icon-down-open"></i>
                                <h5>Who should I contact if I have any questions?</h5>
                            </div>
                            <p>In case of any problems with the completeness of the order, please contact our Contact
                                Center by phone </br><span class="ttg-fw--bold">+777-2345-785</span> or e-mail:<a
                                        href="mailto:info@mydomain.com">info@mydomain.com</a>. Our team will help you
                                solve the problem quickly.</p>
                        </div>
                        <div class="tt-faq__section">
                            <div class="tt-faq__section_head">
                                <i class="icon-down-open"></i>
                                <h5>I don’t want to buy this item now, but I also don’t want to forget it. What should I
                                    do?</h5>
                            </div>
                            <p>You can click the "add to favorites" and item will be added to "your desires". In that
                                case you can return to it purchase later.</p>
                        </div>
                    </div>
                </div>

                <div class="tt-page__name-sm text-center">
                    <h2>Payment</h2>
                </div>

                <div class="tt-page__cont-small">
                    <div class="tt-faq ttg-mb--100">
                        <div class="tt-faq__section">
                            <div class="tt-faq__section_head">
                                <i class="icon-down-open"></i>
                                <h5>Payment methods</h5>
                            </div>
                            <p>Our shop supports multiple payment methods:</p>
                            <p class="ttg-mt--20 ttg-fw--bold">In the case of delivery by courier items can be paid in
                                cash or by card (MasterCard, Visa).</p>
                            <p class="ttg-fw--bold">You can pay by bank transfer.</p>
                            <p class="ttg-fw--bold">Through payment terminals, eWallets, cellular operators.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection