@extends('layouts.mogo')

        <!-- MAIN -->
@section('main_content')
    <main>
        @if ($paginator->hasPages())
            <ul class="tt-page__pagination">
                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                    <li class="tt-page__pagination"><span>? Previous</span></li>
                @else
                    <li><a href="{{ $paginator->previousPageUrl() }}" rel="next">? Previous</a></li>
                @endif
                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="btn tt-pagination__next"><span>{{ $element }}</span></li>
                    @endif
                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="tt-pagination__numbs"><span>{{ $page }}</span></li>
                            @else
                                <li><a href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                         @endforeach
                    @endif

        @endforeach
                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">Next ?</a></li>
                @else
                    <li class="btn tt-pagination__next"><span>Next ?</span></li>
                @endif
            </ul>
        @endif

    </main>
@endsection
