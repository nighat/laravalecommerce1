@extends ('layouts/mogo')


@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>Checkout</h1>
                </div>

                <div class="tt-checkout">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="tt-checkout__steps">
                                <div class="tt-checkout__step-01">
                                    <span>01</span>
                                    <p>Shipping</p>
                                </div>
                                <div class="tt-checkout__step-02">
                                    <span>02</span>
                                    <p>Review & Payments</p>
                                </div>
                            </div>
                            <h4 class="ttg-mt--50 ttg-mb--20">Shipping Address</h4>
                            <div class="tt-checkout__form">
                                <div class="row">
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input tt-input-valid--true">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                            <p class="tt-input__t-valid-true">Text <i class="icon-ok-2"></i></p>
                                            <p class="tt-input__t-valid-false">Text <i class="icon-minus-circled"></i>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input tt-input-valid--false">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                            <p class="tt-input__t-valid-true">Text <i class="icon-ok-2"></i></p>
                                            <p class="tt-input__t-valid-false">Text <i class="icon-minus-circled"></i>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-select">
                                            <select class="form-control">
                                                <option>Text</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-select">
                                            <select class="form-control colorize-theme6-bg">
                                                <option>Text</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-select">
                                            <select class="form-control colorize-theme6-bg">
                                                <option>Text</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="tt-checkout--border ttg-mt--50 ttg-mb--20">Shipping Methods</h4>
                            <div class="tt-checkout__methods">
                                <ul>
                                    <li>
                                        <p>Select Method:</p>
                                        <label class="tt-checkbox-circle">
                                            <input type="checkbox">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <p>Price:</p>
                                        <span>$5.00</span>
                                    </li>
                                    <li>
                                        <p>Method Title:</p>
                                        <span>Fixed</span>
                                    </li>
                                    <li>
                                        <p>Carrier Title:</p>
                                        <span>Flat Rate</span>
                                    </li>
                                </ul>
                            </div>
                            <a href="#" class="btn ttg-mt--20">Next</a>
                            <div class="tt-checkout__steps ttg-mt--100">
                                <div class="tt-checkout__step-03">
                                    <i class="icon-ok-2"></i>
                                    <p>Shipping</p>
                                </div>
                                <div class="tt-checkout__step-01">
                                    <span>02</span>
                                    <p>Review & Payments</p>
                                </div>
                            </div>
                            <h4 class="ttg-mt--60 ttg-mb--30">Payment</h4>
                            <div class="tt-checkout__payment">
                                <ul>
                                    <li>Check / Money order</li>
                                    <li>
                                        <label class="tt-checkbox-circle">
                                            <input type="checkbox">
                                            <span></span>
                                        </label>
                                        My billing and shipping address are the same
                                    </li>
                                    <li>Lorem ipsum dolor sit amet conse</li>
                                    <li>Ctetur adipisicing elit, sed do</li>
                                    <li>Wiusmod tempor incididunt ut labore</li>
                                    <li>Et dolore magna aliqua.</li>
                                </ul>
                            </div>
                            <h4 class="tt-checkout--border ttg-mb--20">Have a Coupon?</h4>
                            <div class="tt-checkout__coupon">`
                                <form action="#">
                                    <label>
                                        <p>Coupone code:</p>
                                        <input type="text" class="form-control" placeholder="Code">
                                    </label>
                                    <button type="submit" class="btn">Apply</button>
                                </form>
                            </div>
                            <div class="tt-checkout--border">
                                <a href="#" class="tt-checkout__btn-order btn btn-type--icon colorize-btn6"><i class="icon-check"></i><span>PlaceOrder</span></a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="tt-summary">
                                <div class="tt-summary--border">
                                    <h4>Order Summary</h4>
                                </div>
                                <div class="tt-summary__products tt-summary--border">
                                    <ul>
                                        <li>
                                            <div>
                                                <a href="#"><img src="images/summary/summary-01.jpg"
                                                                 alt="Image name"></a>
                                            </div>
                                            <div>
                                                <p><a href="#">Elegant and fresh. A most attractive mobile...</a></p>
                                                <span class="tt-summary__products_price">
                                    <span class="tt-summary__products_price-count">1</span>
                                    <span>x</span>
                                    <span class="tt-summary__products_price-val">
                                        <span class="tt-price">
                                            <span>$25</span>
                                        </span>
                                    </span>
                                </span>
                                                <div class="tt-summary__products_param-control tt-summary__products_param-control--open">
                                                    <span>Details</span>
                                                    <i class="icon-down-open"></i>
                                                </div>
                                                <div class="tt-summary__products_param">
                                                    <span class="tt-summary__products_color">Color: <span>Orange</span></span>
                                                    <span class="tt-summary__products_size">Size: <span>XL</span></span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div>
                                                <a href="#"><img src="images/summary/summary-01.jpg"
                                                                 alt="Image name"></a>
                                            </div>
                                            <div>
                                                <p><a href="#">Elegant and fresh. A most attractive mobile...</a></p>
                                                <span class="tt-summary__products_price">
                                    <span class="tt-summary__products_price-count">1</span>
                                    <span>x</span>
                                    <span class="tt-summary__products_price-val">
                                        <span class="tt-price">
                                            <span>$25</span>
                                        </span>
                                    </span>
                                </span>
                                                <div class="tt-summary__products_param-control">
                                                    <span>Details</span>
                                                    <i class="icon-down-open"></i>
                                                </div>
                                                <div class="tt-summary__products_param">
                                                    <span class="tt-summary__products_color">Color: <span>Orange</span></span>
                                                    <span class="tt-summary__products_size">Size: <span>XL</span></span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <script>
                                    require(['app'], function () {
                                        require(['modules/toggleProductParam']);
                                    });
                                </script>
                                <div class="tt-summary--border">
                                    <div class="tt-summary__total">
                                        <p>Subtotal: <span>$78</span></p>
                                    </div>
                                    <div class="tt-summary__total">
                                        <p>Shipping(Flat Rate - Fixed): <span>$78</span></p>
                                    </div>
                                    <div class="tt-summary__total tt-summary__total--m-price-50">
                                        <p>Total: <span>$78</span></p>
                                    </div>
                                </div>
                                <div class="tt-summary__list tt-summary--border">
                                    <h5>Ship To:</h5>
                                    <a href="#"><i class="icon-pencil-circled"></i></a>
                                    <ul>
                                        <li>My billing and shipping address are the same</li>
                                        <li>Lorem ipsum dolor sit amet conse</li>
                                        <li>Ctetur adipisicing elit, sed do</li>
                                        <li>Wiusmod tempor incididunt ut labore</li>
                                        <li>Et dolore magna aliqua.</li>
                                    </ul>
                                </div>
                                <div class="tt-summary__list">
                                    <h5>Shipping Method:</h5>
                                    <a href="#"><i class="icon-pencil-circled"></i></a>
                                    <ul>
                                        <li>My billing and shipping address are the same</li>
                                        <li>Lorem ipsum dolor sit amet conse</li>
                                        <li>Ctetur adipisicing elit, sed do</li>
                                        <li>Wiusmod tempor incididunt ut labore</li>
                                        <li>Et dolore magna aliqua.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
    @endsection


