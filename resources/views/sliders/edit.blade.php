@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel-heading"><h5 style="text-align: center">Slider Edit Form</h5></div>


                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('sliders.update',$slider->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="image" class="col-md-4 control-label">Image</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image" value="{{ $slider->image }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contant" class="col-md-4 control-label">Contant</label>

                            <div class="col-md-6">
                                <input id="contant" type="text" class="form-control" name="contant" value="{{ $slider->contant }}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="link" class="col-md-4 control-label">Link</label>

                            <div class="col-md-6">
                                <input id="link" type="text" class="form-control" name="link" value="{{ $slider->link }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="html_blog" class="col-md-4 control-label">HTML_block</label>

                            <div class="col-md-6">
                                <input id="html_blog" type="text" class="form-control" name="html_blog" value="{{ $slider->html_blog }}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="created_by" class="col-md-4 control-label">Created By</label>

                            <div class="col-md-6">
                                <input id="created_by" type="text" class="form-control" name="created_by" value="{{ $slider->created_by }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="updated_by" class="col-md-4 control-label">Updated By</label>

                            <div class="col-md-6">
                                <input id="updated_by" type="text" class="form-control" name="updated_by" value="{{ $slider->updated_by }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">

                                <button type="submit" class="btn btn-primary">Submit</button>

                                <a href="{{ route('sliders.create') }}" class=" btn btn-danger">Create</a>
                                <a href="{{ route('sliders.index') }}" class=" btn btn-danger">Slider</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection