

@extends('layouts/mogo')

@section('main_content')
       <div class="container">
              <div class="row">
                     <div class="col-md-10 col-md-offset-2">


                                   <div class="panel-heading ">
                                          <h5 style="text-align: center">Slider Details
                                                 <a href="{{ route('sliders.create') }}" class=" btn btn-danger pull-right">Create</a>
                                                 <a href="{{ route('sliders.index') }}" class=" btn btn-danger pull-right">Slider</a>
                                          </h5>

                                   </div>

                                   <div> <b>Image:-</b>   {{ $sliders->image}} </div>
                                   <div> <b>Contant:-</b>  {{ $sliders->contant}}</div>
                                   <div> <b>Link:-</b>  {{ $sliders->link}}</div>
                                   <div> <b>HTML Block:-</b>   {{ $sliders->html_blog}}</div>
                                   <div> <b>Create By:-</b> {{ $sliders->created_by}}</div>
                                  <div> <b>Update By:-</b>  {{ $sliders->updated_by}}</div>


                   </div>

              </div>
       </div>





@endsection