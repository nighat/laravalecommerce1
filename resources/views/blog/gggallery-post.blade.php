<!DOCTYPE html>
@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')
                <div class="tt-page__cont-small">
                    <div class="tt-post-head">
                        <a class="tt-post-head__category" href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                        <div class="tt-post-head__title">Nostrud exercitation ullamco laboris nisi ut aliquip.</div>
                        <div class="tt-post-head__info"><span>Robert</span> on December 28, 2017</div>
                    </div>
                </div>
                <div class="tt-post-slider">
                    <img src="images/blog/single/blog-single-03.jpg" alt="Image name">
                    <img src="images/blog/single/blog-single-04.jpg" alt="Image name">
                    <img src="images/blog/single/blog-single-05.jpg" alt="Image name">
                </div>
                <div class="tt-page__cont-small">
                    <div class="tt-post-text">
                        <p>Dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                            ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                            velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet conse ctetur
                            adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                            fugiat nulla pariatur.</p>
                        <p class="ttg-fw--bold">Dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris...</p>
                        <p>Voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet conse
                            ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                            commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <div class="tt-post-text">
                        <div class="tt-post-text__footer">
                            <div class="tt-post-text__tags">
                                <i class="icon-tag-1"></i>
                                <a href="index.html?page=listing-with-custom-html-block.html">Audio,</a>
                                <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                            </div>
                            <div class="tt-social-icons tt-social-icons--style-04">
                                <a href="#" class="tt-btn">
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-gplus"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-instagram-1"></i>
                                </a>
                                <a href="#" class="tt-btn">
                                    <i class="icon-youtube-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="tt-post-user">
                        <a href="#" class="tt-post-user__image ttg-image-scale"><img
                                    src="images/blog/single/blog-single-user.jpg" alt="Image name"></a>
                        <div class="tt-post-user__text">
                            <div class="tt-post-user__name">Robert Trump</div>
                            <p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                                aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                pariatur. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod
                                tempor incididun</p>
                        </div>
                    </div>
                </div>
                <div class="tt-post-nav__wrap">
                    <div class="tt-page__cont-small">
                        <div class="tt-post-nav">
                            <a href="index.html?page=blog-standart-post.html" class="tt-post-nav__prev">
                                <i class="icon-left-open-big"></i>
                                <div>
                                    <span>Prew post</span>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        doloremqu.</p>
                                </div>
                            </a>
                            <a href="index.html?page=blog-standart-post.html" class="tt-post-nav__next">
                                <div>
                                    <span>Next post</span>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        doloremqu.</p>
                                </div>
                                <i class="icon-right-open-big"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tt-page__cont-small">
                    <div class="tt-comments tt-post__comments">
                        <div class="tt-comments__title">3 Comments</div>
                        <!--comment-->
                        <div class="tt-comments__section">
                            <a class="tt-comments__image" href="#"><img src="images/comments/comments-01.jpg"
                                                                        alt="Image name"></a>
                            <div>
                                <div class="tt-comments__info"><span>Robert</span> on December 28, 2017</div>
                                <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem
                                    aperiam, eaque ipsa quae ab.</p>
                                <a href="#" class="tt-comments__reply">Reply</a>
                                <!--comment-->
                                <div class="tt-comments__section">
                                    <a class="tt-comments__image" href="#"><img src="images/comments/comments-01.jpg"
                                                                                alt="Image name"></a>
                                    <div>
                                        <div class="tt-comments__info"><span>Robert</span> on December 28, 2017</div>
                                        <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                                            totam rem aperiam, eaque ipsa quae ab.</p>
                                        <a href="#" class="tt-comments__reply">Reply</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--comment-->
                        <div class="tt-comments__section">
                            <a class="tt-comments__image" href="#"><img src="images/comments/comments-01.jpg"
                                                                        alt="Image name"></a>
                            <div>
                                <div class="tt-comments__info"><span>Robert</span> on December 28, 2017</div>
                                <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem
                                    aperiam, eaque ipsa quae ab.</p>
                                <a href="#" class="tt-comments__reply">Reply</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tt-page__cont-small">
                    <div class="tt-form tt-post__review">
                        <div class="tt-form__title">Leave a Reply</div>
                        <p class="ttg__required">Your email address will not be published.</p>
                        <div class="tt-form__form">
                            <label>
                                <div class="row">
                                    <div class="col-md-2">
                                        <span class="ttg__required">Name:</span>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control colorize-theme6-bg" placeholder="Enter your name" required="required">
                                    </div>
                                </div>
                            </label>
                            <label>
                                <div class="row">
                                    <div class="col-md-2">
                                        <span class="ttg__required">E-mail:</span>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control colorize-theme6-bg" placeholder="John.smith@example.com" required="required">
                                    </div>
                                </div>
                            </label>
                            <label>
                                <div class="row">
                                    <div class="col-md-2">
                                        <span>Website:</span>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control colorize-theme6-bg" placeholder="Your website">
                                    </div>
                                </div>
                            </label>
                            <label>
                                <div class="row">
                                    <div class="col-md-2">
                                        <span class="ttg__required">Comment:</span>
                                    </div>
                                    <div class="col-md-10">
                                        <textarea class="colorize-theme6-bg">Wtite your comments here</textarea>
                                    </div>
                                </div>
                            </label>
                            <div class="row">
                                <div class="col-md-10 offset-md-2">
                                    <button class="btn btn--xs-flw">Submit Review</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    require(['app'], function () {
                        require(['modules/sliderBlogSingle']);
                    });
                </script>


            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection