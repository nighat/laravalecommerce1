@extends('layouts/mogo')

@section('main_content')
    <div class="panel-heading">
        <h5 style="text-align: center">Page Lists <a href="{{ route('page.create') }}" class=" btn btn-danger pull-right">Add New Page</a></h5>
        <h5 >{{ session('message') }}</h5>


    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Page Title</th>
            <th>Page Content</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach($pages as $page)

            <tr>
                <td>{{ $loop->index + 1}}</td>

                
                <td>{{ $page->page_title }}</td>
                <td>{{ $page->page_contant }}</td>
                <td> <a href="{{ route('page.show',$page->id) }}" class=" btn btn-danger"><span class=" glyphicon glyphicon-eye-open"></span></a>
                    <a href="{{ route('page.edit',$page->id) }}" class=" btn btn-info"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $page->id }}" method="POST" action="{{ route('page.destroy',$page->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $page->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-info"><span class="glyphicon glyphicon-trash"></span></a>

                </td>


            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th>S.No</th>
            <th>Page Title</th>
            <th>Page Content</th>
            <th>Action</th>



        </tr>
        </tfoot>
    </table>



@endsection