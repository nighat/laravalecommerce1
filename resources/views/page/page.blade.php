@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                    <div class="panel-heading"><h3 style="text-align: center">Page Create Form</h3>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('page.store') }}">
                            {{ csrf_field() }}


                             <div class="form-group">
                                <label for="page_title" class="col-md-4 control-label">Page Title</label>

                                <div class="col-md-6">
                                    <input id="page_title" type="text" class="form-control" name="page_title" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="page_contant" class="col-md-4 control-label">Page Contant</label>

                                <div class="col-md-6">
                                     <textarea id="page_contant"  class="form-control" name="page_contant" ></textarea>
                                </div>
                            </div>


                             <div class="form-group">
                                <label for="created_by" class="col-md-4 control-label">Created By</label>

                                <div class="col-md-6">
                                    <input id="created_by" type="text" class="form-control" name="created_by" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="updated_by" class="col-md-4 control-label">Updated By</label>

                                <div class="col-md-6">
                                    <input id="updated_by" type="text" class="form-control" name="updated_by" >
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('page.index') }}" class=" btn btn-danger">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>

            </div>
        </div>
    </div>
@endsection