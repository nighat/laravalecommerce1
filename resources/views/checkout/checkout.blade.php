@extends ('layouts/mogo')


@section('main_content')

        <!-- MAIN -->
<main xmlns="http://www.w3.org/1999/html">

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                {{--@include('include/breadcrumbs')--}}

                <div class="tt-page__name text-center">
                    <h1>Checkout</h1>
                </div>

                <div class="tt-checkout">
                    <div class="row">
                        <div class="col-lg-7">

                            <h4 class="ttg-mt--50 ttg-mb--20">Shipping Address</h4>

                            <form class="form-horizontal" method="POST" action="{{ route('checkout.store') }}">
                                {{ csrf_field() }}

                            <div class="tt-checkout__form">
                                <div class="form-group">


                                    <div class="col-md-6">
                                        <input id="order_id" type="hidden" class="form-control" name="order_id" value="{{ rand() }}" placeholder="Order Id">
                                    </div>
                                </div>
                            </div>
                                <p><span class="ttg__required" > required field</span></p>

                                <div class="tt-checkout__form">
                                <div class="form-group">
                                    <div class="col-md-2"><p><span class="ttg__required"></span> Name:</p></div>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control colorize-theme6-bg" name="name">

                                    </div>
                                </div>
                            </div>
                            <div class="tt-checkout__form">
                                <div class="form-group">
                                    <div class="col-md-2"><p><span class="ttg__required"></span> Email:</p></div>

                                    <div class="col-md-6">
                                        <input id="email" type="text" class="form-control colorize-theme6-bg" name="email" >
                                    </div>
                                </div>
                            </div>
                            <div class="tt-checkout__form">
                                <div class="form-group">
                                    <div class="col-md-2"><p><span class="ttg__required"></span> Address:</p></div>

                                    <div class="col-md-6">
                                        <textarea id="address" class="form-control colorize-theme6-bg" name="address" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tt-checkout__form">
                                <div class="form-group">
                                    <div class="col-md-2"><p><span class="ttg__required"></span> Phone:</p></div>

                                    <div class="col-md-6">
                                        <input id="phone" type="text" class="form-control colorize-theme6-bg" name="phone" placeholder="Example:+8801XXXXXXXXXX">
                                    </div>
                                </div>
                            </div>

                       <h4 class="tt-checkout--border ttg-mt--50 ttg-mb--20"><span class="ttg__required"></span> Payment Methods</h4>
                            <div class="tt-checkout__methods">
                                <ul>
                                    <li>
                                        <div class="form-check" >
                                            <input class="form-check-input" type="radio" name="paymentMethod" id="cashOnDelivery" value="cashOnDelivery" checked>
                                            <label class="form-check-label" for="cashOnDelivery">
                                                <p>Cash On Delivery</p>
                                            </label>
                                        </div>
                                        </li>
                                    <li>
                                        <div class="form-check ">
                                            <input class="form-check-input" type="radio" name="paymentMethod" id="bkash" value="bkash" >
                                            <label class="form-check-label" for="bkash">
                                                <p><img src="images/bkash.png"></p>
                                            </label>
                                        </div>

                                    </li>

                                </ul>
                            </div>

                            <div class="tt-checkout__form hidden" id="transactionNumber">
                                <div class="row">
                                    <div class="col-md-2"><p>Transaction Number:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="number" name="transaction_Number" class="form-control colorize-theme6-bg" placeholder="Transaction Number">
                                        </div>
                                    </div>

                                </div>
                            </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">

                                        <button type="submit" class="tt-header__cart-checkout btn colorize-btn2"><i class="icon-check"></i>Place Order </button>

                                    </div>
                                </div>
                        </form>
                            </div>


                        <div class="col-lg-5">
                            <div class="tt-summary">
                                <div class="tt-summary--border">
                                    @if(count($products)==0)
                                        <h6>Your Are No Order Product</h6>
                                    @elseif(count($products)>0)
                                        <h4 style="text-align: center;color:darkgreen">Order Summary</h4>
                                    @endif

                                </div>

                                <?php $sum=0;?>
                                <div class="tt-summary__products tt-summary--border">
                                    <ul> @foreach($carts as $cart)
                                            @foreach($products as $product)
                                                @if($cart->product_id==$product->id && $cart->session_id==$guestid)

                                                    <li>
                                                        <?php
                                                        $total=$product->price*$cart->qty;
                                                        $sum=$sum+$total;
                                                        ?>

                                                        <div>
                                                            <a href="{{ route('product.detail',$product->id) }}"><img src="{{asset('uploads/images/'.$product->image)}}"
                                                                             alt="Image name"></a>
                                                        </div>
                                                        <div>
                                                            <p>{{$product->description}}</p>
                                                <span class="tt-summary__products_price">
                                    <span class="tt-summary__products_price-count">{{ $cart->qty }}</span>
                                    <span>x</span>
                                    <span class="tt-summary__products_price-val">
                                        <span class="tt-price">
                                            <span>{{ $product->price }}</span>
                                        </span>
                                    </span>
                                </span>
                                                   </div>
                                                    </li>
                                                @endif
                                            @endforeach
                                        @endforeach

                                    </ul>
                                </div>
                                <script>
                                    require(['app'], function () {
                                        require(['modules/toggleProductParam']);
                                    });
                                </script>
                                <div class="tt-summary--border">

                                    <div class="tt-summary__total">
                                        <p style="text-align: center;color:darkgreen">Subtotal: BDT:{{ number_format($sum,2,'.',',').'/-' }}</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection


@push('scripts')
<script>
// var paymentMethod = $('input[name="paymentMethod"]:checked').val();
//    alert(paymentMethod);
    $('#bkash').on('click',function(){

        $('#transactionNumber').removeClass('hidden');

    });
   $('#cashOnDelivery').on('click',function(){

       $('#transactionNumber').addClass('hidden');


    });
</script>
@endpush

