@extends ('layouts/mogo')


@section('main_content')

        <!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>
                        Orders

                    </h1>
                </div>
                </div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>E-mali</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Payment Method</th>
                        <th>Transaction Number</th>
                        <th>Status</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($checkouts as $checkout)

                        <tr>
                            <td>{{ $loop->index + 1}}</td>
                            <td> {{ $checkout->name }}</td>
                            <td>{{ $checkout->email }}</td>
                            <td>{{ $checkout->address }}</td>
                            <td>{{ $checkout->phone }}</td>
                            <td>{{ $checkout->paymentMethod }}</td>
                            <td>
                                {{--@if(empty($checkout->transaction_Number) )--}}

                                    {{--@elseif($checkout->transaction_Number)--}}
                                    {{----}}


                                    {{--@endif--}}
                                {{ $checkout->transaction_Number}}

                            </td>
                            <td>
                                @if($checkout->status==0)

                                    Pending
                                @elseif($checkout->status==1)
                                    Verified
                                @else($checkout->status==2)
                                    Delivered
                                @endif


                            </td>
                            <td>
                                <table>
                                    <tr>
                            <td> <form action="{{route( 'checkout.update',$checkout->id) }}" method="POST" >
                                    {{ csrf_field() }}
                                    {{method_field('PUT')}}
                                     <div class="checkbox full-left">
                                        <label>
                                            <input type="hidden" name="status" value="1">
                                            <button type="submit" class="btn btn-success">Verified</button>
                                        </label>

                                    </div>


                                </form>

                            </td>

                                <td><form action="{{route( 'checkout.update',$checkout->id) }}" method="POST" >
                                    {{ csrf_field() }}
                                    {{method_field('PUT')}}
                                     <div class="checkbox full-left">
                                         <label>
                                             <input type="hidden" name="status" value="2">
                                             <button type="submit" class="btn btn-success">Delivered</button>
                                         </label>

                                    </div>


                                </form>
                            </td>
                            <td>

                                <a  href="" class="btn btn-warning">Detail</a>


                            </td>
                                    </tr>
                                    </table>
                            </td>
                            </tr>

                            </td>

                        </tr>
                    @endforeach

                    </tbody>

                </table>
            
                {{--{{ $checkout->links() }}--}}
            </div>
        </div>
    </div>


    </main>
@endsection
