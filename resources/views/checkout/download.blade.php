
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="invoice-box">
    <h3 style="text-align: center;color: #0044cc">Product Invoice</h3>
    <table style="width:100%; border: 2px solid #eee;" >
        <tr style="float:left;">
            <td colspan="5">
                <table>
                    <tr>
                        {{--<td class="title">

                        </td>--}}

                        <td>
                            <p><img src="{{public_path().'/images/ecomerce.png'}}" style="width:150px; height:90px;"></p>
                            <p style="color: #0044cc">Order #: {{$userInfo['order_id']}}</p>
                            <p>Order Create Time : {{$userInfo['created_at']}}</p>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr style="float:left;">
            <td colspan="5">
                <table style="width:100%">
                    <tr>
                        <td style="width:50%;">
                            <p >Sold to</p>
                            <p>{{$userInfo['name']}}</p>
                            <p>{{$userInfo['address']}}</p>
                            <p>{{$userInfo['email']}}</p>
                            <p>{{$userInfo['phone']}}</p>
                        </td>

                        <td  style="width:50%">
                            <p>Ship to</p>
                            <p>{{$userInfo['name']}}</p>
                            <p>{{$userInfo['address']}}</p>
                            <p>{{$userInfo['email']}}</p>
                            <p>{{$userInfo['phone']}}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr style="width:100%;">

            <td style="width:50%;">Payment Method</td>
            <td colspan="4" style="width:50%">{{$userInfo['paymentMethod']}}</td>

        </tr>

        <table style="width:100%;border: 2px solid #eee;">

            <tr style="text-align: center;color: #0044cc">
                <td>Product</td>
                <td>Picture</td>
                <td>Price</td>
                <td>Qty</td>
                <td>Subtotal</td>
            </tr>

            <?php $grandTotal=0;?>
            @foreach($orderItems as $orderItem)
                @foreach($products as $product)
                    @if($product->id == $orderItem->product_id)
                        <?php $subtotal=$orderItem->price*$orderItem->qty;
                        $grandTotal+=$subtotal;
                        ?>
                        <tr style="text-align: center">
                            <td>{{$product->title}}</td>
                            <td><img style="height: 100px; width: 100px" src="{{public_path().'/uploads/images/'.$product->image}}"></td>
                            <td>{{number_format($orderItem->price,2,'.',',').'/-'}}</td>
                            <td>{{$orderItem->qty}}</td>
                            <td>{{number_format($subtotal,2,'.',',').'/-'}}</td>
                        </tr>
                    @endif
                @endforeach
            @endforeach

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>GrandTotal</td>
                <td>{{number_format($grandTotal,2,'.',',').'/-'}}</td>
            </tr>
        </table>



    </table>
    <br/>
    <p style="color: #0044cc">Special Note::</p>
    <b>This is software generated document. It does not require any signature.</b>

</div>
</body>
</html>
