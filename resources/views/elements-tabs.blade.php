@extends ('layouts/mogo')


@section('main_content')


    <!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>Tabs</h1>
                </div>

                <div class="tt-page__cont-small ttg-mt--60 ttg-mb--90">
                    <div class="ttg-mb--70">
                        <div class="tt-tabs tt-tabs-test tt-layout__mobile-full" data-tt-type="horizontal">
                            <div class="tt-tabs__head">
                                <div class="tt-tabs__slider">
                                    <div class="tt-tabs__btn" data-active="true"><span>Tab 1</span></div>
                                    <div class="tt-tabs__btn"><span>Tab 2</span></div>
                                    <div class="tt-tabs__btn"><span>Tab 3</span></div>
                                </div>
                                <div class="tt-tabs__btn-prev"></div>
                                <div class="tt-tabs__btn-next"></div>
                                <div class="tt-tabs__border"></div>
                            </div>
                            <div class="tt-tabs__body tt-tabs-product">
                                <div>
                                    <span>Tab 1 <i class="icon-down-open"></i></span>
                                    <div class="tt-tabs__content">
                                        <div class="tt-tabs__content-head">Title</div>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                            doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
                                            ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                                        <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                                            totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                            architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                                            quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                                            dolores.</p>
                                        <ul class="tt-tabs-product__list">
                                            <li><span>Omnis iste natus error sit voluptatem</span></li>
                                            <li><span>Accusantium doloremque</span></li>
                                            <li><span>Laudantium, totam rem aperiam,</span></li>
                                            <li><span>Eaque ipsa quae ab illo inventore veritatis</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div>
                                    <span>Tab 2 <i class="icon-down-open"></i></span>
                                    <div class="tt-tabs__content">
                                        <div class="tt-tabs__content-head">Title</div>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                            doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
                                            ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                                        <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                                            totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                            architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                                            quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                                            dolores.</p>
                                        <ul class="tt-tabs-product__list">
                                            <li><span>Omnis iste natus error sit voluptatem</span></li>
                                            <li><span>Accusantium doloremque</span></li>
                                            <li><span>Laudantium, totam rem aperiam,</span></li>
                                            <li><span>Eaque ipsa quae ab illo inventore veritatis</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div>
                                    <span>Tab 3 <i class="icon-down-open"></i></span>
                                    <div class="tt-tabs__content">
                                        <div class="tt-tabs__content-head">Title</div>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                            doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
                                            ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                                        <p>Omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                                            totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                            architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                                            quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                                            dolores.</p>
                                        <ul class="tt-tabs-product__list">
                                            <li><span>Omnis iste natus error sit voluptatem</span></li>
                                            <li><span>Accusantium doloremque</span></li>
                                            <li><span>Laudantium, totam rem aperiam,</span></li>
                                            <li><span>Eaque ipsa quae ab illo inventore veritatis</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="tt-tabs tt-tabs-test tt-layout__mobile-full" data-tt-type="vertical">
                            <div class="tt-tabs__head">
                                <div class="tt-tabs__slider">
                                    <div class="tt-tabs__btn" data-active="true"><span>Tab 1</span></div>
                                    <div class="tt-tabs__btn"><span>Tab 2</span></div>
                                    <div class="tt-tabs__btn"><span>Tab 3</span></div>
                                </div>
                                <div class="tt-tabs__btn-prev"></div>
                                <div class="tt-tabs__btn-next"></div>
                                <div class="tt-tabs__border"></div>
                            </div>
                            <div class="tt-tabs__body tt-tabs-my-account">
                                <div>
                                    <span>Tab 1 <i class="icon-down-open"></i></span>
                                    <div class="tt-tabs__content">
                                        <div class="tt-tabs__content-head">Title</div>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                            doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
                                            ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                                        <ul class="tt-tabs-product__list">
                                            <li><span>Omnis iste natus error sit voluptatem</span></li>
                                            <li><span>Accusantium doloremque</span></li>
                                            <li><span>Laudantium, totam rem aperiam,</span></li>
                                            <li><span>Eaque ipsa quae ab illo inventore veritatis</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div>
                                    <span>Tab 2 <i class="icon-down-open"></i></span>
                                    <div class="tt-tabs__content">
                                        <div class="tt-tabs__content-head">Title</div>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                            doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
                                            ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                                        <ul class="tt-tabs-product__list">
                                            <li><span>Omnis iste natus error sit voluptatem</span></li>
                                            <li><span>Accusantium doloremque</span></li>
                                            <li><span>Laudantium, totam rem aperiam,</span></li>
                                            <li><span>Eaque ipsa quae ab illo inventore veritatis</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div>
                                    <span>Tab 3 <i class="icon-down-open"></i></span>
                                    <div class="tt-tabs__content">
                                        <div class="tt-tabs__content-head">Title</div>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                            doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
                                            ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                                        <ul class="tt-tabs-product__list">
                                            <li><span>Omnis iste natus error sit voluptatem</span></li>
                                            <li><span>Accusantium doloremque</span></li>
                                            <li><span>Laudantium, totam rem aperiam,</span></li>
                                            <li><span>Eaque ipsa quae ab illo inventore veritatis</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    require(['app'], function () {
                        require(['modules/tabs']);
                    });
                </script>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection