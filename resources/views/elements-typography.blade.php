@extends ('layouts/mogo')


@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>Typography</h1>
                </div>

                <div class="tt-page__cont-small ttg-mt--60">
                    <div class="ttg-mb--80">
                        <h4 class="ttg-mb--40">Headlines</h4>
                        <h1 class="ttg-mb--30">H1 Heading</h1>
                        <p class="ttg-mb--40">That immediately brought to mind one of my fondest memories, involving my
                            daughter when she was just a toddler of one: taking her with me on the short walk to check
                            the mail. I live in a small enclave of homes in which all the mailboxes are together in a
                            central location, less than a minute’s walk from my front door…when I walk alone, that is.
                            When I would take my daughter with me it was easily 20 minutes.</p>
                        <h2 class="ttg-mb--30">H2 Heading</h2>
                        <p class="ttg-mb--40">That immediately brought to mind one of my fondest memories, involving my
                            daughter when she was just a toddler of one: taking her with me on the short walk to check
                            the mail. I live in a small enclave of homes in which all the mailboxes are together in a
                            central location, less than a minute’s walk from my front door…when I walk alone, that is.
                            When I would take my daughter with me it was easily 20 minutes.</p>
                        <h3 class="ttg-mb--20">H3. Heading</h3>
                        <p class="ttg-mb--40">That immediately brought to mind one of my fondest memories, involving my
                            daughter when she was just a toddler of one: taking her with me on the short walk to check
                            the mail. I live in a small enclave of homes in which all the mailboxes are together in a
                            central location, less than a minute’s walk from my front door…when I walk alone, that is.
                            When I would take my daughter with me it was easily 20 minutes.</p>
                        <h4 class="ttg-mb--20">H4. Heading</h4>
                        <p class="ttg-mb--40">That immediately brought to mind one of my fondest memories, involving my
                            daughter when she was just a toddler of one: taking her with me on the short walk to check
                            the mail. I live in a small enclave of homes in which all the mailboxes are together in a
                            central location, less than a minute’s walk from my front door…when I walk alone, that is.
                            When I would take my daughter with me it was easily 20 minutes.</p>
                        <h5 class="ttg-mb--10">H5. Heading</h5>
                        <p class="ttg-mb--40">That immediately brought to mind one of my fondest memories, involving my
                            daughter when she was just a toddler of one: taking her with me on the short walk to check
                            the mail. I live in a small enclave of homes in which all the mailboxes are together in a
                            central location, less than a minute’s walk from my front door…when I walk alone, that is.
                            When I would take my daughter with me it was easily 20 minutes.</p>
                        <h6 class="ttg-mb--10">H6. Heading</h6>
                        <p class="ttg-mb--40">That immediately brought to mind one of my fondest memories, involving my
                            daughter when she was just a toddler of one: taking her with me on the short walk to check
                            the mail. I live in a small enclave of homes in which all the mailboxes are together in a
                            central location, less than a minute’s walk from my front door…when I walk alone, that is.
                            When I would take my daughter with me it was easily 20 minutes.</p>
                    </div>
                    <div class="ttg-mb--40">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="ttg-mb--20">Unordered List</h4>
                                <ul class="ttg-list ttg-mb--40">
                                    <li>Omnis iste natus error sit voluptatem
                                        <ul>
                                            <li>Accusantium doloremque</li>
                                        </ul>
                                    </li>
                                    <li>Laudantium, totam rem aperiam,</li>
                                    <li>Eaque ipsa quae ab illo inventore veritatis</li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <h4 class="ttg-mb--20">Ordered List</h4>
                                <ol class="ttg-list ttg-mb--40">
                                    <li>Omnis iste natus error sit voluptatem
                                        <ol>
                                            <li>Accusantium doloremque</li>
                                        </ol>
                                    </li>
                                    <li>Laudantium, totam rem aperiam,</li>
                                    <li>Eaque ipsa quae ab illo inventore veritatis</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="ttg-mb--80">
                        <h4 class="ttg-mb--20">Some Other Styles</h4>
                        <p class="ttg-capitalize ttg-capitalize--c-theme ttg-capitalize--small">Lorem ipsum dolor sit
                            amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
                            magna aliqua Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                            aliquip Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                            in reprehenderit in voluptate.</p>
                        <p class="ttg-fw--bold">Bold Text: Lorem ipsum dolor sit amet conse ctetur adipisicing</p>
                        <p class="ttg-text--line-through">Strike-through: Lorem ipsum dolor sit amet conse ctetur
                            adipisicing</p>
                        <a href="#" class="ttg-text--underline ttg-mb--10">Link: Lorem ipsum dolor sit amet conse ctetur
                            adipisicing</a>
                        <p class="ttg-text--italic">Italic Text: Lorem ipsum dolor sit amet conse ctetur adipisicing</p>
                        <p class="ttg-text--highlight">Inline Code: Lorem ipsum dolor sit amet conse ctetur
                            adipisicing</p>
                    </div>
                    <div class="ttg-mb--90">
                        <h4 class="ttg-mb--20">Blockquotes</h4>
                        <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate.</p>
                        <div class="ttg-quote ttg-mt--40 ttg-mb--40">
                            <div><i class="icon-quote-1"></i></div>
                            <div>
                                <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod
                                    tempor...</p>
                                <span>— Robert Trump</span>
                            </div>
                        </div>
                        <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection