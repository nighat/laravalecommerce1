
@extends ('layouts/mogo')


@section('main_content')
<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                <div class="tt-comming-soon">
                    <img src="images/coming-soon/coming-soon-01.png" alt="Image name">
                    <div class="container-fluid">
                        <div class="tt-comming-soon__content">
                            <h1>We’re Coming Soon</h1>
                            <h4>Subscribe to Our Newsletter</h4>
                            <p class="ttg-fw--bold ttg-color--theme ttg-mt--20">Sign up for our e-mail and be the first
                                who know our special offers!</br>Furthermore, we will give a 15% discount on the next
                                order after you sign up.</p>
                            <form action="#" class="tt-comming-soon__newsletter tt-newsletter tt-newsletter--btn-dark">
                                <input type="email" name="email" class="form-control"
                                       placeholder="Enter please your e-mail">
                                <button type="submit" class="btn">
                                    <i class="tt-newsletter__text-wait"></i>
                                    <span class="tt-newsletter__text-default">Subscribe!</span>
                                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                                </button>
                            </form>
                            <div class="tt-comming-soon__countdown"
                                 data-date="2017-12-01"
                                 data-year="Years"
                                 data-month="Months"
                                 data-week="Weeks"
                                 data-day="Days"
                                 data-hour="Hours"
                                 data-minute="Minutes"
                                 data-second="Seconds"></div>
                            <div class="tt-comming-soon__copyright"><p>&copy; 2017 . All Rights Reserved.</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection