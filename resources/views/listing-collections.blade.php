@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">

                <div class="container-fluid ttg-cont-padding--none">
                    <div class="row ttg-grid-padding--none">
                        <div class="col-md-4 col-lg-4">
                            <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-text-animation-parent
                   ttg-image-translate--right
                   ttg-animation-disable--md
                   tt-promobox__hover-disable--md">
                                <div class="tt-promobox__content">
                                    <img data-srcset="images/promoboxes/promobox-05.jpg" alt="Image name">
                                    <div class="tt-promobox__text"
                                         data-resp-md="md"
                                         data-resp-sm="md"
                                         data-resp-xs="sm">
                                        <div>Watches</div>
                                    </div>
                                    <div class="tt-promobox__hover
                    tt-promobox__hover--fade">
                                        <div class="tt-promobox__hover-bg"></div>
                                        <div class="tt-promobox__text tt-promobox__point-lg--center">
                                            <div class="ttg-text-animation--emersion">
                                                <span>Watches</span>
                                            </div>
                                            <p class="ttg-text-animation--emersion">
                                                <span><span>28</span> products</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a></div>
                        <div class="col-md-4 col-lg-4">
                            <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-text-animation-parent
                   ttg-image-scale
                   ttg-animation-disable--md
                   tt-promobox__hover-disable--md">
                                <div class="tt-promobox__content">
                                    <img data-srcset="images/promoboxes/promobox-06.jpg" alt="Image name">
                                    <div class="tt-promobox__text
                    "
                                         data-resp-md="md"
                                         data-resp-sm="md"
                                         data-resp-xs="sm">
                                        <div>Trackers</div>
                                    </div>
                                    <div class="tt-promobox__hover
                    tt-promobox__hover--fade">
                                        <div class="tt-promobox__hover-bg"></div>
                                        <div class="tt-promobox__text
                        tt-promobox__point-lg--center
                        ">
                                            <div class="ttg-text-animation--emersion">
                                                <span>Trackers</span>
                                            </div>
                                            <p class="ttg-text-animation--emersion">
                                                <span><span>28</span> products</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a></div>
                        <div class="col-md-4 col-lg-4">
                            <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-text-animation-parent
                   ttg-image-translate--bottom
                   ttg-animation-disable--md
                   tt-promobox__hover-disable--md">
                                <div class="tt-promobox__content">
                                    <img data-srcset="images/promoboxes/promobox-07.jpg" alt="Image name">
                                    <div class="tt-promobox__text"
                                         data-resp-md="md"
                                         data-resp-sm="lg"
                                         data-resp-xs="sm">
                                        <div>Power Banks</div>
                                    </div>
                                    <div class="tt-promobox__hover
                    tt-promobox__hover--fade">
                                        <div class="tt-promobox__hover-bg"></div>
                                        <div class="tt-promobox__text
                        tt-promobox__point-lg--center">
                                            <div class="ttg-text-animation--emersion">
                                                <span>Power Banks</span>
                                            </div>
                                            <p class="ttg-text-animation--emersion">
                                                <span><span>28</span> products</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a></div>
                        <div class="col-md-4 col-lg-4">
                            <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-text-animation-parent
                   ttg-image-scale
                   ttg-animation-disable--md
                   tt-promobox__hover-disable--md">
                                <div class="tt-promobox__content">
                                    <img data-srcset="images/promoboxes/promobox-08.jpg" alt="Image name">
                                    <div class="tt-promobox__text"
                                         data-resp-md="md"
                                         data-resp-sm="lg"
                                         data-resp-xs="sm">
                                        <div class="colorize-theme2-c">Earphones</div>
                                    </div>
                                    <div class="tt-promobox__hover
                    tt-promobox__hover--fade">
                                        <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                        <div class="tt-promobox__text
                        tt-promobox__point-lg--center">
                                            <div class="ttg-text-animation--emersion">
                                                <span class="colorize-theme2-c">Earphones</span>
                                            </div>
                                            <p class="ttg-text-animation--emersion">
                                                <span class="colorize-theme2-c"><span>28</span> products</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a></div>
                        <div class="col-md-4 col-lg-4">
                            <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-text-animation-parent
                   ttg-image-translate--top
                   ttg-animation-disable--md
                   tt-promobox__hover-disable--md">
                                <div class="tt-promobox__content">
                                    <img data-srcset="images/promoboxes/promobox-09.jpg" alt="Image name">
                                    <div class="tt-promobox__text
                    "
                                         data-resp-md="md"
                                         data-resp-sm="lg"
                                         data-resp-xs="sm">
                                        <div class="colorize-theme2-c">Headphones</div>
                                    </div>
                                    <div class="tt-promobox__hover
                    tt-promobox__hover--fade">
                                        <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                        <div class="tt-promobox__text
                        tt-promobox__point-lg--center
                        ">
                                            <div class="ttg-text-animation--emersion">
                                                <span class="colorize-theme2-c">Headphones</span>
                                            </div>
                                            <p class="ttg-text-animation--emersion">
                                                <span class="colorize-theme2-c"><span>28</span> products</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a></div>
                        <div class="col-md-4 col-lg-4">
                            <a href="index.html?page=listing-with-custom-html-block.html" class="tt-promobox
                   tt-promobox__size-square
                   ttg-text-animation-parent
                   ttg-image-translate--left
                   ttg-animation-disable--md
                   tt-promobox__hover-disable--md">
                                <div class="tt-promobox__content">
                                    <img data-srcset="images/promoboxes/promobox-10.jpg" alt="Image name">
                                    <div class="tt-promobox__text"
                                         data-resp-md="md"
                                         data-resp-sm="lg"
                                         data-resp-xs="sm">
                                        <div class="colorize-theme2-c">Speakers</div>
                                    </div>
                                    <div class="tt-promobox__hover
                    tt-promobox__hover--fade">
                                        <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                        <div class="tt-promobox__text
                        tt-promobox__point-lg--center">
                                            <div class="ttg-text-animation--emersion">
                                                <span class="colorize-theme2-c">Speakers</span>
                                            </div>
                                            <p class="ttg-text-animation--emersion">
                                                <span class="colorize-theme2-c"><span>28</span> products</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection