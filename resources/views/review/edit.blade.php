@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                    <div class="panel-heading"><h5 style="text-align: center">Review Edit Form</h5></div>


                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('review.update',$review->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            
                             <div class="form-group">
                                <label for="detail" class="col-md-4 control-label">Detail</label>

                                <div class="col-md-6">
                                    <input id="detail" type="text" class="form-control" name="detail" value="{{ $review->detail }}" >
                                </div>
                            </div>
                            
                             <div class="form-group">
                                <label for="product_id" class="col-md-4 control-label">Product Id</label>

                                <div class="col-md-6">
                                    <input id="product_id" type="text" class="form-control" name="product_id" value="{{ $review->product_id }}" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="created_by" class="col-md-4 control-label">Created By</label>

                                <div class="col-md-6">
                                    <input id="created_by" type="text" class="form-control" name="created_by" value="{{ $review->created_by }}">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="updated_by" class="col-md-4 control-label">Updated By</label>

                                <div class="col-md-6">
                                    <input id="updated_by" type="text" class="form-control" name="updated_by" value="{{ $review->updated_by }}">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button type="submit" class="btn btn-primary">Submit</button>

                                    <a href="{{ route('review.create') }}" class=" btn btn-danger">Create</a>
                                    <a href="{{ route('review.index') }}" class=" btn btn-danger">Review</a>
                                </div>
                            </div>
                        </form>
                    </div>

            </div>
        </div>
    </div>
@endsection