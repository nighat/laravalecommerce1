@extends('layouts/mogo')

@section('main_content')
    <div class="panel-heading">
        <h5 style="text-align: center">Review Lists <a href="{{ route('review.create') }}" class=" btn btn-danger pull-right">Add New Review</a></h5>
        <div style="background:#00b38f; color: #ffffff; width: 600px;text-align: center; font-size: 20px;">{{ session('message') }}</div


    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Detail</th>
            <th>Product Id</th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach($reviews as $review)

            <tr>
                <td>{{ $loop->index + 1}}</td>


                <td>{{ $review->detail }}</td>
                <td>{{ $review->product_id }}</td>
                <td>{{ $review->created_by}}</td>
                <td>{{ $review->updated_by}}</td>

                <td> <a href="{{ route('review.show',$review->id) }}" class=" btn btn-danger"><span class=" glyphicon glyphicon-eye-open"></span></a>
                    <a href="{{ route('review.edit',$review->id) }}" class=" btn btn-info"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $review->id }}" method="POST" action="{{ route('review.destroy',$review->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $review->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-info"><span class="glyphicon glyphicon-trash"></span></a>

                </td>


            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th>S.No</th>
            <th>Detail</th>
            <th>Product Id</th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Action</th>


        </tr>
        </tfoot>
    </table>

    {{ $reviews->links() }}

@endsection