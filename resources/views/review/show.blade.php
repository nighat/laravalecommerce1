

@extends('layouts/mogo')

@section('main_content')
       <div class="container">
              <div class="row">
                     <div class="col-md-10 col-md-offset-2">


                                   <div class="panel-heading ">
                                          <h5 style="text-align: center">Review Details
                                                 <a href="{{ route('review.create') }}" class=" btn btn-danger pull-right">Create</a>
                                                 <a href="{{ route('review.index') }}" class=" btn btn-danger pull-right">Review</a>
                                          </h5>

                                   </div>

                                   <div> <b>Detail:-</b>   {{ $review->detail}} </div>
                                   <div> <b>Product Id:-</b>  {{ $review->product_id}}</div>
                                   <div> <b>Create By:-</b> {{ $review->created_by}}</div>
                                  <div> <b>Update By:-</b>  {{ $review->updated_by}}</div>


                   </div>

              </div>
       </div>





@endsection