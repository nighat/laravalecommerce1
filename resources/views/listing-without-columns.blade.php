@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                <div class="tt-listing-page">
                    @include('include/breadcrumbs')
                    <div class="tt-listing-page__category-name"><h1>Audio</h1></div>
                    <div class="tt-listing-page__view-options tt-vw-opt">
                        <div class="row">
                            <div class="col-xl-6 col-lg-5 col-md-5 col-xs-12">
                                <div class="tt-vw-opt__sort">
                                    <span>Sort:</span>
                                    <label class="tt-select">
                                        <select class="form-control">
                                            <option>Default</option>
                                            <option>Default #2</option>
                                            <option>Default #3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="tt-vw-opt__direction">
                                    <a href="#" class="active"><i class="icon-down"></i></a>
                                    <a href="#"><i class="icon-up"></i></a>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-5 col-xs-8">
                                <div class="tt-vw-opt__info">
                                    <span>Showing 1–9 of 23 results</span>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-2 col-xs-4">
                                <div class="tt-vw-opt__length">
                                    <span>Show:</span>
                                    <label class="tt-select">
                                        <select class="form-control">
                                            <option>12</option>
                                            <option>9</option>
                                            <option>6</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="tt-vw-opt__grid">
                                    <div class="tt-product-btn-vw" data-control=".tt-product-view">
                                        <label>
                                            <input type="radio" name="product-btn-vw" checked>
                                            <i class="icon-th-large"></i>
                                            <i class="icon-check-empty"></i>
                                        </label>
                                        <label>
                                            <input type="radio" name="product-btn-vw"
                                                   data-view-class="tt-product-view--list">
                                            <i class="icon-th"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tt-listing-page__products tt-layout__mobile-full">

                        <div class="tt-product-view row">
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-01.jpg"
                                                 data-retina="images/products/product-01.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                        <div class="tt-product__labels">
                                            <span class="tt-label__new">New</span>
                                            <span class="tt-label__hot">Hot</span>

                                            <span class="tt-label__sale">Sale</span>
                                            <span class="tt-label__discount">$22</span>

                                            <div><span class="tt-label__out-stock">Out Stock</span></div>
                                            <div><span class="tt-label__in-stock">In Stock</span></div>
                                        </div>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Headphones</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$22</span>
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                            </div>

                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__stars tt-stars">
                                                    <span class="ttg-icon"></span>
                                                    <span class="ttg-icon" style="width:86%;"></span>
                                                </div>
                                            </div>
                                            <div class="tt-product__option">
                                                <div class="prdbut__options prdbut__options--list">
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="prdbut__option prdbut__option--color prdbut__option--design-color">
                                                        <span class="prdbut__val prdbut__val--white active default">
                                                            <span>White</span>
                                                        </span>
                                                            <span class="prdbut__val prdbut__val--black">
                                                            <span>Black</span>
                                                        </span>
                                                            <span class="prdbut__val prdbut__val--red">
                                                            <span>Red</span>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="prdbut__option prdbut__option--size prdbut__option--design-bg">
                                                        <span class="prdbut__val prdbut__val--m active default">
                                                            <span>M</span>
                                                        </span>
                                                            <span class="prdbut__val prdbut__val--l">
                                                            <span>L</span>
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart tt-btn__state--active">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like tt-btn__state--active">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare tt-btn__state--active">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__countdown" data-date="2018-10-01 20:00:00" data-zone="Europe/Madrid"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-02.jpg"
                                                 data-retina="images/products/product-02.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Phone</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$46</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__stars tt-stars">
                                                    <span class="ttg-icon"></span>
                                                    <span class="ttg-icon" style="width:35%;"></span>
                                                </div>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-03.jpg"
                                                 data-retina="images/products/product-03.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                        <div class="tt-product__labels">
                                            <span class="tt-label__sale">Sale</span>
                                            <span class="tt-label__discount">$32</span>
                                        </div>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">USB</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$32</span>
                                                    <span>$38</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__countdown" data-date="2018-06-01"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-04.jpg"
                                                 data-retina="images/products/product-04.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$14</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-05.jpg"
                                                 data-retina="images/products/product-05.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$24</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-06.jpg"
                                                 data-retina="images/products/product-06.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-07.jpg"
                                                 data-retina="images/products/product-07.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$58</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-08.jpg"
                                                 data-retina="images/products/product-08.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$19</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-09.jpg"
                                                 data-retina="images/products/product-09.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$24</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-10.jpg"
                                                 data-retina="images/products/product-10.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-11.jpg"
                                                 data-retina="images/products/product-11.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$58</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-4 col-xxl-3">
                                <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                    <div class="tt-product__image">
                                        <a href="product-simple-variant-1.html">
                                            <img src="images/loader.svg" data-srcset="images/products/product-12.jpg"
                                                 data-retina="images/products/product-12.jpg"
                                                 alt="Elegant and fresh. A most attractive mobile power supply.">
                                        </a>
                                    </div>
                                    <div class="tt-product__hover tt-product__clr-clk-transp">
                                        <div class="tt-product__content">
                                            <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                            </h3>
                                            <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                            </p>
                                            <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                            <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$19</span>
                                                </span>
                                            </span>
                                            </div>
                                            <div class="ttg-text-animation--emersion">
                                                <div class="tt-product__buttons">
                                                    <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                        <i class="icon-shop24"></i>
                                                        <span>Add to Cart</span>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                        <i class="icon-heart-empty-2"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                        <i class="icon-untitled-1"></i>
                                                    </a>
                                                    <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                        <i class="icon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tt-page__pagination">
                        <div class="tt-pagination">
                            <a href="#" class="btn tt-pagination__prev ttg-hidden">Prev</a>
                            <div class="tt-pagination__numbs">
                                <span>1</span>
                                <span><a href="#">2</a></span>
                                <span><a href="#">3</a></span>
                            </div>
                            <a href="#" class="btn tt-pagination__next">Next</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection