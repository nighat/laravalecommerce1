@extends('layouts/mogo')

@section('main_content')
    <div class="panel-heading">
        <h5 style="text-align: center">
            Staticblock Lists
            <a href="{{ route('staticblock.create') }}" class=" btn btn-danger pull-right">Add New Static Block</a>
        </h5>
        <div style="background:#00b38f; color: #ffffff; width: 600px;text-align: center; font-size: 20px;">{{ session('message') }}</div>

    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Title</th>
            <th>contant</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach($staticblocks as $staticblock)

            <tr>
                <td>{{ $loop->index + 1}}</td>
                <td>{{ $staticblock->title}}</td>
                <td>{{ $staticblock->contant }}</td>
                <td> <a href="{{ route('staticblock.show',$staticblock->id) }}" class=" btn btn-danger"><span class=" glyphicon glyphicon-eye-open"></span></a>

                    <a href="{{ route('staticblock.edit',$staticblock->id) }}" class=" btn btn-info"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $staticblock->id }}" method="POST" action="{{ route('staticblock.destroy',$staticblock->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $staticblock->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-info"><span class="glyphicon glyphicon-trash"></span></a>

                </td>


            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th>S.No</th>
            <th>Title</th>
            <th>contant</th>
            <th>Action</th>



        </tr>
        </tfoot>
    </table>

    {{ $staticblocks->links() }}

@endsection