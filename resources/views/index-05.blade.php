@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                <div class="tt-sr tt-sr__nav-v2 tt-sr__bullets--off" data-layout="fullscreen">
                    <div class="tt-sr__content" data-version="5.3.0.2">
                        <ul>
                            <li data-index="rs-3045"
                                data-transition="parallaxvertical"
                                data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                                data-masterspeed="1500">
                                <img src="images/slider/05/slide-01.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                    text-center
                    rs-parallaxlevel-3
                    tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[-100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="300">
                                    <div>Headphone</div>
                                    <span>$64</span>
                                    <p>Superior sound quality</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3046"
                                data-transition="fade"
                                data-masterspeed="1500">
                                <img src="images/slider/05/slide-02.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-kenburns="on"
                                     data-duration="10000"
                                     data-ease="Linear.easeNone"
                                     data-scalestart="120"
                                     data-scaleend="100"
                                     data-offsetstart="0 0"
                                     data-offsetend="0 0"
                                     data-rotatestart="0"
                                     data-rotateend="0"
                                     data-bgparallax="8">
                                <div class="tp-caption
                    text-center
                    rs-parallaxlevel-3
                    tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"1999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                     data-x="center"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']">
                                    <div>Speaker</div>
                                    <span>$178</span>
                                    <p>Portable Bluetooth stereo speakers</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3047"
                                data-transition="parallaxhorizontal"
                                data-masterspeed="1500">
                                <img src="images/slider/05/slide-03.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                    text-center
                    rs-parallaxlevel-3
                    tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-x="right"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="300">
                                    <div>Speaker</div>
                                    <span>$72</span>
                                    <p>Portable Bluetooth stereo speakers</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3048"
                                data-transition="zoomout"
                                data-masterspeed="1500">
                                <img src="images/slider/05/slide-04.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                    text-center
                    rs-parallaxlevel-3
                    tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"1999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                     data-x="center"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']">
                                    <div>Smart Watche</div>
                                    <span>$473</span>
                                    <p>Just what you need. Right when you need it.</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3049"
                                data-transition="parallaxvertical"
                                data-masterspeed="1500">
                                <img src="images/slider/05/slide-05.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                    text-center
                    rs-parallaxlevel-3
                    tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-x="right"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="300">
                                    <div>Headphone</div>
                                    <span>$349</span>
                                    <p>Superior sound quality</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3050"
                                data-transition="fade"
                                data-masterspeed="1500">
                                <img src="images/slider/05/slide-06.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-kenburns="on"
                                     data-duration="6000"
                                     data-ease="Linear.easeNone"
                                     data-scalestart="120"
                                     data-scaleend="100"
                                     data-offsetstart="0 0"
                                     data-offsetend="0 0"
                                     data-rotatestart="0"
                                     data-rotateend="0"
                                     data-bgparallax="8">
                                <div class="tp-caption
                    text-center
                    rs-parallaxlevel-3
                    tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="300">
                                    <div>Smart Watches</div>
                                    <span>$273</span>
                                    <p>Just what you need. Right when you need it.</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="tt-home__promobox-03">

                    <div class="container">
                        <div class="row ttg-grid-pdg-btm--sm">
                            <div class="col-md-4 col-lg-12 col-xl-4">
                                <div class="tt-promobox
                                            ttg-image-translate--top
                                            ttg-animation-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-14.jpg" alt="Image name">
                                        <div class="tt-promobox__hover tt-promobox__hover--up">
                                            <div class="tt-promobox__hover-bg
                                                        tt-promobox__hover-bg--visible
                                                        ttg-bg--theme">
                                            </div>
                                            <div class="tt-promobox__text
                                                        tt-promobox__point-lg--top
                                                        tt-promobox__point-lg--left">
                                                <a href="listing-with-custom-html-block.html" class="colorize-theme2-c">Audio</a>
                                                <ul>
                                                    <li><a href="#">Wireless</a></li>
                                                    <li><a href="#">Built-In Microphone</a></li>
                                                    <li><a href="#">Bluetooth Enabled</a></li>
                                                    <li><a href="#">Rechargeable</a></li>
                                                    <li><a href="#">USB Device Charging</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-12 col-xl-4">
                                <div class="tt-promobox
                                            ttg-image-translate--right
                                            ttg-animation-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-15.jpg" alt="Image name">
                                        <div class="tt-promobox__hover tt-promobox__hover--up">
                                            <div class="tt-promobox__hover-bg
                                                        tt-promobox__hover-bg--visible
                                                        ttg-bg--theme">
                                            </div>
                                            <div class="tt-promobox__text
                                                        tt-promobox__point-lg--top
                                                        tt-promobox__point-lg--left">
                                                <a href="listing-with-custom-html-block.html" class="colorize-theme2-c">Watches</a>
                                                <ul>
                                                    <li><a href="#">Wireless</a></li>
                                                    <li><a href="#">Built-In Microphone</a></li>
                                                    <li><a href="#">Bluetooth Enabled</a></li>
                                                    <li><a href="#">Rechargeable</a></li>
                                                    <li><a href="#">USB Device Charging</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-12 col-xl-4">
                                <div class="tt-promobox
                                            ttg-image-translate--bottom
                                            ttg-animation-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-16.jpg" alt="Image name">
                                        <div class="tt-promobox__hover tt-promobox__hover--up">
                                            <div class="tt-promobox__hover-bg
                                                        tt-promobox__hover-bg--visible
                                                        ttg-bg--theme">
                                            </div>
                                            <div class="tt-promobox__text
                                                        tt-promobox__point-lg--top
                                                        tt-promobox__point-lg--left">
                                                <a href="listing-with-custom-html-block.html" class="colorize-theme2-c">Trackers</a>
                                                <ul>
                                                    <li><a href="#">Wireless</a></li>
                                                    <li><a href="#">Built-In Microphone</a></li>
                                                    <li><a href="#">Bluetooth Enabled</a></li>
                                                    <li><a href="#">Rechargeable</a></li>
                                                    <li><a href="#">USB Device Charging</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="tt-page__section ttg-mb--40">
                        <div class="tt-page__section-head tt-page__section-head--center tt-page__section-head--arrows">
                            <div class="tt-page__title">Featured Products</div>
                            <div class="tt-page__arrows tt-page__arrows--in-head">
                                <span class="tt-page__arrows-prev"><i class="icon-left-open-2"></i></span>
                                <span class="tt-page__arrows-next"><i class="icon-right-open-2"></i></span>
                            </div>
                        </div>
                        <div class="tt-carousel-box">
                            <div class="tt-product-view">
                                <div class="tt-carousel-box__slider">
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-01.jpg"
                                                         data-retina="images/products/product-01.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                                <div class="tt-product__labels">
                                                    <span class="tt-label__new">New</span>
                                                    <span class="tt-label__hot">Hot</span>

                                                    <span class="tt-label__sale">Sale</span>
                                                    <span class="tt-label__discount">$22</span>

                                                    <div><span class="tt-label__out-stock">Out Stock</span></div>
                                                    <div><span class="tt-label__in-stock">In Stock</span></div>
                                                </div>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Headphones</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$22</span>
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                                    </div>

                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__stars tt-stars">
                                                            <span class="ttg-icon"></span>
                                                            <span class="ttg-icon" style="width:86%;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart tt-btn__state--active">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like tt-btn__state--active">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare tt-btn__state--active">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-02.jpg"
                                                         data-retina="images/products/product-02.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Phone</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$46</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__stars tt-stars">
                                                            <span class="ttg-icon"></span>
                                                            <span class="ttg-icon" style="width:35%;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-03.jpg"
                                                         data-retina="images/products/product-03.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                                <div class="tt-product__labels">
                                                    <span class="tt-label__sale">Sale</span>
                                                    <span class="tt-label__discount">$32</span>
                                                </div>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">USB</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$32</span>
                                                    <span>$38</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-04.jpg"
                                                         data-retina="images/products/product-04.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$14</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-05.jpg"
                                                         data-retina="images/products/product-05.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$24</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-06.jpg"
                                                         data-retina="images/products/product-06.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-07.jpg"
                                                         data-retina="images/products/product-07.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$58</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                            <div class="tt-product__image">
                                                <a href="product-simple-variant-1.html">
                                                    <img src="images/loader.svg" data-srcset="images/products/product-08.jpg"
                                                         data-retina="images/products/product-08.jpg"
                                                         alt="Elegant and fresh. A most attractive mobile power supply.">
                                                </a>
                                            </div>
                                            <div class="tt-product__hover tt-product__clr-clk-transp">
                                                <div class="tt-product__content">
                                                    <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                    </h3>
                                                    <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                    </p>
                                                    <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                                        error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                                        aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                                    <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$19</span>
                                                </span>
                                            </span>
                                                    </div>
                                                    <div class="ttg-text-animation--emersion">
                                                        <div class="tt-product__buttons">
                                                            <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                                <i class="icon-shop24"></i>
                                                                <span>Add to Cart</span>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                                <i class="icon-heart-empty-2"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                                <i class="icon-untitled-1"></i>
                                                            </a>
                                                            <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                                <i class="icon-eye"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="tt-home__carousel-product">

                        <div class="row">
                            <div class="col-xl-4">
                                <div class="tt-page__section">
                                    <div class="tt-page__section-head tt-page__section-head--arrows">
                                        <div class="tt-page__title">New Products</div>
                                        <div class="tt-page__arrows tt-page__arrows--in-head tt-page__arrows--vertical">
                                            <span class="tt-page__arrows-prev"><i class="icon-left-open-2"></i></span>
                                            <span class="tt-page__arrows-next"><i class="icon-right-open-2"></i></span>
                                        </div>
                                    </div>
                                    <div class="tt-carousel-box tt-carousel-box--vertical">
                                        <div class="tt-product-view tt-product-view--list tt-product-view--preview">
                                            <div class="tt-carousel-box__slider">
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-01.jpg"
                                                                     data-retina="images/products/product-01.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                            <div class="tt-product__labels">
                                                                <span class="tt-label__sale">Sale</span>
                                                                <span class="tt-label__discount">$22</span>
                                                            </div>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Headphones</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$22</span>
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                                                </div>

                                                                <div class="ttg-text-animation--emersion">
                                                                    <div class="tt-product__stars tt-stars">
                                                                        <span class="ttg-icon"></span>
                                                                        <span class="ttg-icon" style="width:86%;"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-02.jpg"
                                                                     data-retina="images/products/product-02.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Phone</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$46</span>
                                                </span>
                                            </span>
                                                                </div>
                                                                <div class="ttg-text-animation--emersion">
                                                                    <div class="tt-product__stars tt-stars">
                                                                        <span class="ttg-icon"></span>
                                                                        <span class="ttg-icon" style="width:35%;"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-03.jpg"
                                                                     data-retina="images/products/product-03.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                            <div class="tt-product__labels">
                                                                <span class="tt-label__sale">Sale</span>
                                                                <span class="tt-label__discount">$32</span>
                                                            </div>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">USB</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$32</span>
                                                    <span>$38</span>
                                                </span>
                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-04.jpg"
                                                                     data-retina="images/products/product-04.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$14</span>
                                                </span>
                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="tt-page__section">
                                    <div class="tt-page__section-head tt-page__section-head--arrows">
                                        <div class="tt-page__title">Special Products</div>
                                        <div class="tt-page__arrows tt-page__arrows--in-head tt-page__arrows--vertical">
                                            <span class="tt-page__arrows-prev"><i class="icon-left-open-2"></i></span>
                                            <span class="tt-page__arrows-next"><i class="icon-right-open-2"></i></span>
                                        </div>
                                    </div>
                                    <div class="tt-carousel-box tt-carousel-box--vertical">
                                        <div class="tt-product-view tt-product-view--list tt-product-view--preview">
                                            <div class="tt-carousel-box__slider">
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-05.jpg"
                                                                     data-retina="images/products/product-05.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$24</span>
                                                </span>
                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-06.jpg"
                                                                     data-retina="images/products/product-06.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-07.jpg"
                                                                     data-retina="images/products/product-07.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$58</span>
                                                </span>
                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-08.jpg"
                                                                     data-retina="images/products/product-08.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$19</span>
                                                </span>
                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="tt-page__section">
                                    <div class="tt-page__section-head tt-page__section-head--arrows">
                                        <div class="tt-page__title">Most Products</div>
                                        <div class="tt-page__arrows tt-page__arrows--in-head tt-page__arrows--vertical">
                                            <span class="tt-page__arrows-prev"><i class="icon-left-open-2"></i></span>
                                            <span class="tt-page__arrows-next"><i class="icon-right-open-2"></i></span>
                                        </div>
                                    </div>
                                    <div class="tt-carousel-box tt-carousel-box--vertical">
                                        <div class="tt-product-view tt-product-view--list tt-product-view--preview">
                                            <div class="tt-carousel-box__slider">
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-09.jpg"
                                                                     data-retina="images/products/product-09.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                            <div class="tt-product__labels">
                                                                <span class="tt-label__sale">Sale</span>
                                                                <span class="tt-label__discount">$22</span>
                                                            </div>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Headphones</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$22</span>
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                                                </div>

                                                                <div class="ttg-text-animation--emersion">
                                                                    <div class="tt-product__stars tt-stars">
                                                                        <span class="ttg-icon"></span>
                                                                        <span class="ttg-icon" style="width:86%;"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-10.jpg"
                                                                     data-retina="images/products/product-02.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Phone</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$46</span>
                                                </span>
                                            </span>
                                                                </div>
                                                                <div class="ttg-text-animation--emersion">
                                                                    <div class="tt-product__stars tt-stars">
                                                                        <span class="ttg-icon"></span>
                                                                        <span class="ttg-icon" style="width:35%;"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-11.jpg"
                                                                     data-retina="images/products/product-03.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                            <div class="tt-product__labels">
                                                                <span class="tt-label__sale">Sale</span>
                                                                <span class="tt-label__discount">$32</span>
                                                            </div>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">USB</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$32</span>
                                                    <span>$38</span>
                                                </span>
                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                                        <div class="tt-product__image">
                                                            <a href="product-simple-variant-1.html">
                                                                <img src="images/loader.svg" data-srcset="images/products/product-12.jpg"
                                                                     data-retina="images/products/product-04.jpg"
                                                                     alt="Elegant and fresh. A most attractive mobile power supply.">
                                                            </a>
                                                        </div>
                                                        <div class="tt-product__hover tt-product__clr-clk-transp">
                                                            <div class="tt-product__content">
                                                                <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block.html">Category</a>
                                            </span>
                                                                </h3>
                                                                <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1.html">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                                                </p>
                                                                <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$14</span>
                                                </span>
                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="tt-home__brands-02">
                        <div class="tt-carousel-brands__wrap">
                            <div class="tt-carousel-brands">
                                <a href="#"><img data-srcset="images/brands/carousel/brand-01.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-02.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-03.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-04.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-05.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-06.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-07.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-08.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-01.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-02.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-03.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-04.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-05.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-06.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-07.png" alt="Image name"></a>
                                <a href="#"><img data-srcset="images/brands/carousel/brand-08.png" alt="Image name"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="tt-page__section">
                        <div class="tt-page__section-head tt-page__section-head--center tt-page__section-head--arrows">
                            <div class="tt-page__title">From the Blog</div>
                            <div class="tt-page__arrows tt-page__arrows--in-head">
                                <span class="tt-page__arrows-prev"><i class="icon-left-open-2"></i></span>
                                <span class="tt-page__arrows-next"><i class="icon-right-open-2"></i></span>
                            </div>
                        </div>
                        <div class="tt-home__carousel-blog">

                            <div class="tt-carousel-box tt-layout__mobile-full">
                                <div class="tt-carousel-box__slider" data-grid="4">
                                    <div class="col-xl-12">
                                        <div class="tt-post-grid">
                                            <a href="#" class="tt-post-grid__image ttg-image-translate--left">
                                                <img data-srcset="images/blog/carousel/blog-carousel-01.jpg"
                                                     alt="Image name">
                                            </a>
                                            <div class="tt-post-grid__content">
                                                <div class="tt-post-grid__category">
                                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                                </div>
                                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis
                                                    iste natus.</a>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque laudantium, totam rem aperiam.</p>
                                                <div class="tt-post-grid__footer">
                                                    <div class="tt-post-grid__info">
                                                        <span>Robert</span> on December 28, 2017
                                                        <div class="tt-post-grid__tags">
                                                            <i class="icon-tag-1"></i>
                                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="tt-post-grid__comments">
                                                        <i class="icon-comment-empty"></i><span>7</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="tt-post-grid">
                                            <div class="tt-post-grid__image">
                                                <div class="tt-post-grid__slider">
                                                    <img data-srcset="images/blog/carousel/blog-carousel-02.jpg"
                                                         alt="Image name">
                                                    <img data-srcset="images/blog/grid/blog-grid-02.jpg"
                                                         alt="Image name">
                                                    <img data-srcset="images/blog/grid/blog-grid-04.jpg"
                                                         alt="Image name">
                                                </div>
                                                <div class="tt-post-grid__slider-nav"></div>
                                            </div>
                                            <div class="tt-post-grid__content">
                                                <div class="tt-post-grid__category">
                                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                                </div>
                                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis
                                                    iste natus.</a>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque laudantium, totam rem aperiam.</p>
                                                <div class="tt-post-grid__footer">
                                                    <div class="tt-post-grid__info">
                                                        <span>Robert</span> on December 28, 2017
                                                        <div class="tt-post-grid__tags">
                                                            <i class="icon-tag-1"></i>
                                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="tt-post-grid__comments">
                                                        <i class="icon-comment-empty"></i><span>7</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="tt-post-grid">
                                            <a href="#" class="tt-post-grid__image ttg-image-translate--top">
                                                <img data-srcset="images/blog/carousel/blog-carousel-03.jpg"
                                                     alt="Image name">
                                            </a>
                                            <div class="tt-post-grid__content">
                                                <div class="tt-post-grid__category">
                                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                                </div>
                                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis
                                                    iste natus.</a>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque laudantium, totam rem aperiam.</p>
                                                <div class="tt-post-grid__footer">
                                                    <div class="tt-post-grid__info">
                                                        <span>Robert</span> on December 28, 2017
                                                        <div class="tt-post-grid__tags">
                                                            <i class="icon-tag-1"></i>
                                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="tt-post-grid__comments">
                                                        <i class="icon-comment-empty"></i><span>7</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="tt-post-grid">
                                            <a href="#" class="tt-post-grid__image ttg-image-translate--right">
                                                <img data-srcset="images/blog/carousel/blog-carousel-04.jpg"
                                                     alt="Image name">
                                            </a>
                                            <div class="tt-post-grid__content">
                                                <div class="tt-post-grid__category">
                                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                                </div>
                                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis
                                                    iste natus.</a>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque laudantium, totam rem aperiam.</p>
                                                <div class="tt-post-grid__footer">
                                                    <div class="tt-post-grid__info">
                                                        <span>Robert</span> on December 28, 2017
                                                        <div class="tt-post-grid__tags">
                                                            <i class="icon-tag-1"></i>
                                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="tt-post-grid__comments">
                                                        <i class="icon-comment-empty"></i><span>7</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="tt-post-grid">
                                            <a href="#" class="tt-post-grid__image ttg-image-translate--bottom">
                                                <img data-srcset="images/blog/carousel/blog-carousel-05.jpg"
                                                     alt="Image name">
                                            </a>
                                            <div class="tt-post-grid__content">
                                                <div class="tt-post-grid__category">
                                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                                </div>
                                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis
                                                    iste natus.</a>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque laudantium, totam rem aperiam.</p>
                                                <div class="tt-post-grid__footer">
                                                    <div class="tt-post-grid__info">
                                                        <span>Robert</span> on December 28, 2017
                                                        <div class="tt-post-grid__tags">
                                                            <i class="icon-tag-1"></i>
                                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="tt-post-grid__comments">
                                                        <i class="icon-comment-empty"></i><span>7</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="tt-post-grid">
                                            <a href="#" class="tt-post-grid__image ttg-image-translate--bottom">
                                                <img data-srcset="images/blog/carousel/blog-carousel-06.jpg"
                                                     alt="Image name">
                                            </a>
                                            <div class="tt-post-grid__content">
                                                <div class="tt-post-grid__category">
                                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                                </div>
                                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis
                                                    iste natus.</a>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque laudantium, totam rem aperiam.</p>
                                                <div class="tt-post-grid__footer">
                                                    <div class="tt-post-grid__info">
                                                        <span>Robert</span> on December 28, 2017
                                                        <div class="tt-post-grid__tags">
                                                            <i class="icon-tag-1"></i>
                                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="tt-post-grid__comments">
                                                        <i class="icon-comment-empty"></i><span>7</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="tt-post-grid">
                                            <a href="#" class="tt-post-grid__image ttg-image-translate--bottom">
                                                <img data-srcset="images/blog/carousel/blog-carousel-07.jpg"
                                                     alt="Image name">
                                            </a>
                                            <div class="tt-post-grid__content">
                                                <div class="tt-post-grid__category">
                                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                                </div>
                                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis
                                                    iste natus.</a>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                                    accusantium doloremque laudantium, totam rem aperiam.</p>
                                                <div class="tt-post-grid__footer">
                                                    <div class="tt-post-grid__info">
                                                        <span>Robert</span> on December 28, 2017
                                                        <div class="tt-post-grid__tags">
                                                            <i class="icon-tag-1"></i>
                                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="tt-post-grid__comments">
                                                        <i class="icon-comment-empty"></i><span>7</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="tt-home__shipping-info-03">

                        <div class="tt-shp-info tt-shp-info__design-03 tt-shp-info__align--left">
                            <div class="row ttg-grid-padding--none">
                                <div class="col-lg-4">
                                    <a href="#" class="tt-shp-info__section tt-shp-info__align--left ">
                                        <i class="icon-box"></i>
                                        <div>
                                            <div class="tt-shp-info__strong">Free Shipping & Return</div>
                                            <p>Free shipping on all orders over $99.</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#" class="tt-shp-info__section tt-shp-info__align--left ">
                                        <i class="icon-thumbs-up-1"></i>
                                        <div>
                                            <div class="tt-shp-info__strong">Money Back Guarantee</div>
                                            <p>100% money back guarantee.</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#" class="tt-shp-info__section tt-shp-info__align--left ">
                                        <i class="icon-phone"></i>
                                        <div>
                                            <div class="tt-shp-info__strong">Online Support</div>
                                            <p>Free shipping on all orders over $99.</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection