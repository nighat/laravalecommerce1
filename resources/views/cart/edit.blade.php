@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel-heading"><h5 style="text-align: center">Cart Edit Form</h5></div>


                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('cart.update',$cart->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <div class="form-group">
                            <label for="session_id" class="col-md-4 control-label">Session Id</label>

                            <div class="col-md-6">
                                <input id="session_id" type="number" class="form-control" name="session_id" value="{{ $cart->session_id}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_id " class="col-md-4 control-label">product_id </label>

                            <div class="col-md-6">
                                <input id="product_id " type="number" class="form-control" name="product_id " value="{{ $cart->product_id  }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_qty" class="col-md-4 control-label">Product Qty</label>

                            <div class="col-md-6">
                                <input id="product_qty" type="number" class="form-control" name="product_qty" value="{{ $cart->product_qty }}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_price" class="col-md-4 control-label">Product Price</label>

                            <div class="col-md-6">
                                <input id="product_price" type="number" class="form-control" name="product_price" value="{{ $cart->product_price }}">
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">

                                <button type="submit" class="btn btn-primary">Submit</button>

                                <a href="{{ route('cart.create') }}" class=" btn btn-danger">Create</a>
                                <a href="{{ route('cart.index') }}" class=" btn btn-danger">cart</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection