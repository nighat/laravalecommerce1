

@extends('layouts/mogo')

@section('main_content')
       <div class="container">
              <div class="row">
                     <div class="col-md-10 col-md-offset-2">


                                   <div class="panel-heading ">
                                          <h5 style="text-align: center">Cart Details
                                                 <a href="{{ route('cart.create') }}" class=" btn btn-danger pull-right">Create</a>
                                                 <a href="{{ route('cart.index') }}" class=" btn btn-danger pull-right">Cart</a>
                                          </h5>

                                   </div>


                                   <div> <b>Session Id:-</b>  {{ $cart->session_id}}</div>
                                   <div> <b>Product Id:-</b>   {{ $cart->product_id}}</div>
                                   <div> <b>Product Qty:-</b> {{ $cart->product_qty}}</div>
                                  <div> <b>Product Price:-</b>  {{ $cart->product_price}}</div>


                   </div>

              </div>
       </div>





@endsection