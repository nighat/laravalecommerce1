@extends('layouts/mogo')

@section('main_content')
    <div class="panel-heading">
        <h5 style="text-align: center">Cart Lists </h5>
        <h5 >{{ session('message') }}</h5>

    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Product Name</th>
            <th>Qty</th>
            <th>Unit Price</th>
            <th>Subtotal</th>
            <th>&nbsp;</th>


        </tr>
        </thead>
        <tbody>
        <?php $sum = 0;?>
        @foreach($carts as $cart)
            @foreach($products as $product)
                @if($cart->product_id==$product->id && $cart->session_id==$guestid)

            <tr>
             <?php
                $total=$cart->qty*$product->price;
                $sum = $total+$sum;
                ?>
                <td>
                    <img style="height: 100px; width: 100px" src="{{asset('uploads/images/'.$product->image)}} "/>
                    <form action="{{ route('cart.destroy',$cart->id) }}" method="post" style="display: inline">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are You Sure you Want To Remove product ?')"><span class="glyphicon glyphicon-trash"></span></button>
                    </form>
                </td>
                <td><a href="{{ route('product.show',$cart->product_id) }}">{{ $product->title }}</a></td>
                <td>
                    <div class="col-md-2 col-xs-4 text-center">

                        <div class="tt-counter tt-counter__inner" data-min="1" data-max="10">
                            <form action="{{url( '/cart/'.$cart->id) }}" method="POST" >
                                {{ csrf_field() }}
                                {{method_field('PUT')}}

                                <input type="hidden" name="session_id" class="form-control" value="{{ $id }}">
                                <input type="hidden" class="form-control" value="{{ $product->price }}" name="price">
                                <input type="number" class="form-control" value="{{ $cart->qty }}" name="qty" >

                                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span></button>
                            </form>


                        </div>
                    </div>

                </td>
                <td>BDT {{ number_format($product->price ,2,'.',',').'/-'}}</td>
                <td>BDT {{  number_format($total,2,'.',',').'/-' }}</td>

            </tr>
                @endif
            @endforeach
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <td colspan="4"><h6 style="text-align: right">Grand Total</h6></td>
            <td colspan="2"> <h6 style="text-align: left">{{ number_format($sum,2,'.',',') }}</h6> </td>

            {{--<td>//{{$cart->price->count() }}</td>--}}
        </tr>
        <tr>

            <td colspan="3"><a class='btn btn-outline-info' href="{{ route(('product.view'))}}">Shop More</a>
                <a href="{{route("checkout.create")}}"><button  class="tt-header__cart-checkout btn colorize-btn2"><i class="icon-check"></i>Checkout</button></a>
            </td>
            <td colspan="3"></td>



        </tr>
        </tfoot>
    </table>



@endsection
