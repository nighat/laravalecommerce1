@extends ('layouts/mogo')


@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>Checkout</h1>
                </div>

                <div class="tt-checkout">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="tt-checkout__label-link ttg-mb--20">
                                <h4>Returning Customer?</h4>
                                <a href="$">Click Here to Login</a>
                            </div>
                            <p class="ttg-mb--30">If you have shopped with us before, please enter your details in the
                                boxes below. If you are a new customer, please proceed to the Billing & Shipping
                                section.</p>
                            <div class="tt-checkout__form">
                                <div class="row">
                                    <div class="col-md-2"><p class="ttg__required">Username or Email:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text"
                                                   required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p class="ttg__required">Password:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text"
                                                   required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-10 offset-md-2">
                                        <a href="#" class="btn ttg-mt--10">Login</a>
                                    </div>
                                    <div class="col-md-10 offset-md-2">
                                        <label class="tt-checkbox ttg-mt--40">
                                            <input type="checkbox">
                                            <span></span>
                                            <p>Don’t show this popup again</p>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="tt-checkout__label-link tt-checkout--border ttg-mb--20">
                                <h4>RHave a Coupon?</h4>
                                <a href="$">Click Here to Enter Your Code</a>
                            </div>
                            <div class="tt-checkout__coupon">`
                                <form action="#">
                                    <label>
                                        <p>Coupone code:</p>
                                        <input type="text" class="form-control" placeholder="Code">
                                    </label>
                                    <button type="submit" class="btn">Apply</button>
                                </form>
                            </div>
                            <h4 class="tt-checkout--border ttg-mt--50 ttg-mb--20">Billing Address</h4>
                            <div class="tt-checkout__form">
                                <div class="row">
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input tt-input-valid--true">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                            <p class="tt-input__t-valid-true">Text <i class="icon-ok-2"></i></p>
                                            <p class="tt-input__t-valid-false">Text <i class="icon-minus-circled"></i>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input tt-input-valid--false">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                            <p class="tt-input__t-valid-true">Text <i class="icon-ok-2"></i></p>
                                            <p class="tt-input__t-valid-false">Text <i class="icon-minus-circled"></i>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-select">
                                            <select class="form-control colorize-theme6-bg">
                                                <option>Text</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-select">
                                            <select class="form-control colorize-theme6-bg">
                                                <option>Text</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-select">
                                            <select class="form-control colorize-theme6-bg">
                                                <option>Text</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <div class="tt-input">
                                            <input type="text" class="form-control colorize-theme6-bg" placeholder="Text">
                                        </div>
                                    </div>
                                    <div class="col-md-10 offset-md-2">
                                        <label class="tt-checkbox">
                                            <input type="checkbox">
                                            <span></span>
                                            <p>Create an account?</p>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <h4 class="tt-checkout--border ttg-mt--50 ttg-mb--30">Additional Information</h4>
                            <div class="tt-checkout__form">
                                <div class="row">
                                    <div class="col-md-2"><p>Text:</p></div>
                                    <div class="col-md-10">
                                        <textarea class="form-control">Text</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="tt-summary">
                                <div class="tt-summary--border">
                                    <h4>Your Order</h4>
                                </div>
                                <div class="tt-summary__products tt-summary__products--shot-list tt-summary--border">
                                    <ul>
                                        <li>
                                            <p><a href="#">Elegant and fresh. A most attractive mobile...</a></p>
                                            <span class="tt-summary__products_price">
                                <span class="tt-summary__products_price-count">1</span>
                                <span>x</span>
                                <span class="tt-summary__products_price-val">
                                    <span class="tt-price">
                                        <span>$25</span>
                                    </span>
                                </span>
                            </span>
                                        </li>
                                        <li>
                                            <p><a href="#">Elegant and fresh. A most attractive mobile...</a></p>
                                            <span class="tt-summary__products_price">
                                <span class="tt-summary__products_price-count">1</span>
                                <span>x</span>
                                <span class="tt-summary__products_price-val">
                                    <span class="tt-price">
                                        <span>$25</span>
                                    </span>
                                </span>
                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tt-summary__total ttg-mb--20">
                                    <p>Subtotal: <span>$78</span></p>
                                </div>
                                <div class="tt-summary--border">
                                    <div class="row">
                                        <div class="col-xxl-3 col-xl-4 col-lg-12 col-md-2 col-sm-3">
                                            <h5 class="ttg-mb--10">Shipping:</h5>
                                        </div>
                                        <div class="col-xxl-9 col-xl-8 col-lg-12 col-md-10 col-sm-9">
                                            <ul class="tt-summary__categories tt-categories tt-categories__toggle ttg-mb--10">
                                                <li><a href="#">Flat Rate: $12</a></li>
                                                <li><a href="#">Free Shipping</a></li>
                                                <li><a href="#">Flat Rate: $60</a></li>
                                                <li><a href="#">Local Delivery: $5</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tt-summary--border tt-summary__total tt-summary__total--m-price-50">
                                    <p>Total: <span>$78</span></p>
                                </div>
                                <ul class="tt-summary__categories tt-categories tt-categories__toggle">
                                    <li><a href="#">Direct Bank Transfer</a></li>
                                </ul>
                                <p class="ttg-mb--10">If you have shopped with us before, please enter your details in
                                    the boxes below. If you are a new customer, please proceed to the Billing & Shipping
                                    section.</p>
                                <ul class="tt-summary__categories tt-categories tt-categories__toggle ttg-mb--10">
                                    <li><a href="#">Check Payments</a></li>
                                    <li><a href="#">Cash On Delivery</a></li>
                                </ul>
                                <a href="$" class="tt-summary__btn-checkout btn btn-type--icon colorize-btn6"><i
                                            class="icon-check"></i><span>PlaceOrder</span></a>
                            </div>
                            <script>
                                require(['app'], function () {
                                    require(['modules/category']);
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection
