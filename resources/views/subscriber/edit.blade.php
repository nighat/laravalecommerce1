@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                    <div class="panel-heading"><h5 style="text-align: center">Subscriber Edit Form</h5></div>


                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('subscriber.update',$subscriber->id) }}" >
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            
                             <div class="form-group">
                                <label for="mail" class="col-md-4 control-label">HTML_block</label>

                                <div class="col-md-6">
                                    <input id="mail" type="text" class="form-control" name="mail" value="{{ $subscriber->mail }}" >
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button type="submit" class="btn btn-primary">Submit</button>

                                    <a href="{{ route('subscriber.create') }}" class=" btn btn-danger">Create</a>
                                    <a href="{{ route('subscriber.index') }}" class=" btn btn-danger">subscriber</a>
                                </div>
                            </div>
                        </form>
                    </div>

            </div>
        </div>
    </div>
@endsection