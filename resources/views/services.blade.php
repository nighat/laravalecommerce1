@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>Services</h1>
                    <p>Dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
                </div>

                <div class="ttg-mt--90">
                    <div class="ttg-image-scale">
                        <img data-srcset="images/services/services-01.jpg" alt="Image name">
                    </div>
                </div>

                <div class="tt-home__shipping-info-01">

                    <div class="tt-shp-info tt-shp-info__design-04 tt-shp-info__design-striped">
                        <div class="row ttg-grid-padding--none">
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <span class="tt-shp-info__number">1</span>
                                    <div class="tt-shp-info__strong">Adequate Prices</div>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <span class="tt-shp-info__number">2</span>
                                    <div class="tt-shp-info__strong">Ideal Balance Between Price & Quality.</div>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <span class="tt-shp-info__number">3</span>
                                    <div class="tt-shp-info__strong">Saving of Time</div>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row ttg-grid-padding--none">
                    <div class="col-sm-6">
                        <div class="ttg-image-translate--left">
                            <img data-srcset="images/services/services-02.jpg" alt="Image name">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="ttg-image-translate--top">
                            <img data-srcset="images/services/services-03.jpg" alt="Image name">
                        </div>
                    </div>
                </div>

                <div class="tt-page__name-sm text-center ttg-mt--90">
                    <h2>What We Do</h2>
                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore.</p>
                </div>

                <div class="container ttg-mt--50">
                    <div class="row">
                        <div class="col-xl-4 col-lg-6">
                            <div class="tt-post-grid">
                                <a href="#" class="tt-post-grid__image ttg-image-translate--left">
                                    <img data-srcset="images/services/blog/services-blog-01.jpg" alt="Image name">
                                </a>
                                <div class="tt-post-grid__content text-center">
                                    <a href="#" class="tt-post-grid__title">A Wide Selection</a>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                        veritatis.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="tt-post-grid">
                                <a href="#" class="tt-post-grid__image ttg-image-scale">
                                    <img data-srcset="images/services/blog/services-blog-02.jpg" alt="Image name">
                                </a>
                                <div class="tt-post-grid__content text-center">
                                    <a href="#" class="tt-post-grid__title">Optimal Delivery Option</a>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                        veritatis.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6">
                            <div class="tt-post-grid">
                                <a href="#" class="tt-post-grid__image ttg-image-translate--top">
                                    <img data-srcset="images/services/blog/services-blog-03.jpg" alt="Image name">
                                </a>
                                <div class="tt-post-grid__content text-center">
                                    <a href="#" class="tt-post-grid__title">Comfort and Convenience</a>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                        veritatis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tt-page__name-sm text-center ttg-mt--60 ttg-mb--100">
                    <h2>Interested in Working Together?</h2>
                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore.</p>
                    <a href="#" class="btn">Get in touch with us</a>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection