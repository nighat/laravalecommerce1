@extends('layouts/mogo')

@section('main_content')
    <div class="panel-heading">
        <h5 style="text-align: center">
            Product Lists
            <a href="{{ route('product.create') }}" class=" btn btn-danger pull-right">Add New Product</a>
        </h5>
        <div style="background:#00b38f; color: #ffffff; width: 600px;text-align: center; font-size: 20px;">{{ session('message') }}</div>

    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Image</th>
            <th>Title</th>
            <th>Price</th>
            <th>Qty</th>
            <th>SKU</th>
            <th>Product_url</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach($products as $product)

            <tr>
                <td>{{ $loop->index + 1}}</td>
                <td><img style="height:100px; width: 100px" src="{{asset('uploads/images/'.$product->image)}}"></td>
                <td> {{ $product->title }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->qty}}</td>
                <td>{{ $product->sku}}</td>
                <td> {{ $product->product_url}}</td>
                <td> <a href="{{ route('product.show',$product->id) }}" class=" btn btn-info"><span class=" glyphicon glyphicon-eye-open"></span></a>
                    <a href="{{ route('product.edit',$product->id) }}" class=" btn btn-info"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $product->id }}" method="POST" action="{{ route('product.destroy',$product->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $product->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-info"><span class="glyphicon glyphicon-trash"></span></a>

                </td>

            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th>S.No</th>
            <th>Image</th>
            <th>Title</th>
            <th>Price</th>
            <th>Qty</th>
            <th>SKU</th>
            <th>Product_url</th>
            <th>Action</th>



        </tr>
        </tfoot>
    </table>

    {{ $products->links('') }}
@endsection