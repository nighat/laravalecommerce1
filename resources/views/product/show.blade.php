

@extends('layouts/mogo')

@section('main_content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">


    <div class="panel-heading ">
        <h5 style="text-align: center">Product Details
            <a href="{{ route('product.index') }}" class=" btn btn-danger pull-right">Product</a>
            <a href="{{ route('product.create') }}" class=" btn btn-danger pull-right">Create</a>
        </h5>

    </div>

                <div> <b>Image:-</b>  <img style="height: 200px; width: 200px" src="{{asset('uploads/images/'.$product->image)}}"> </div>
       <div>
         <b>Title:- </b>  {{  $product->title}}
       </div>

       <div>
          <b> Short Description:-</b>  {{  $product->short_description}}
       </div>
       <div>
          <b>Description:-</b>  {{  $product->description}}
       </div>
       <div>
           <b>Additional Information:-</b>  {{  $product->additional_information}}
       </div>
       <div>
           <b>Price:-</b>  {{  $product->price}}
       </div>
       <div>
           <b>Special Price:-</b>  {{  $product->special_price}}
       </div>
                <div>
           <b>Qty:-</b>  {{  $product->qty}}
       </div>
       <div>
           <b>Start Date:-</b>  {{  $product->start_date}}
       </div>
       <div>
           <b>End Date:-</b>  {{  $product->end_date}}
       </div>
       <div>
          <b> SKU:-</b>  {{  $product->sku}}
       </div>
       <div>
           <b>Meta Keyword:</b> -  {{  $product->meta_keyword}}
       </div>
       <div>
          <b> Meta Description:-</b>  {{  $product->meta_description}}
       </div>
       <div>
          <b>Product URL:-</b>  {{  $product->product_url}}
       </div>
      <div>
         <b> Created By:- </b>   {{  $product->created_by}}
      </div>
      <div>
          <b>Updated By:-</b>    {{  $product->updated_by}}
      </div>
      <div>
          <b>Created At:-</b>    {{  $product->created_at}}
      </div>
      <div>
         <b> Updated At:-</b>    {{  $product->updated_at}}
      </div>
    </div>
    </div>
    </div>

@endsection