<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function review(){
        return $this->hasMany('App\Review');
    }
    public function picture(){
        return $this->hasMany('App\Picture');
    }
    public function cart()
    {
        return $this->belongsToMany('App\Cart');
    }
    public function checkout()
    {
        return $this->belongsToMany('App\Checkout');
    }
}
