<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function product()
    {
        return $this->belongsToMany('App\Product');
    }
    public function checkout()
    {
        return $this->belongsToMany('App\Checkout');
    }
}
