<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    public function cart()
    {
        return $this->belongsToMany('App\Cart');
    }
    public function product()
    {
        return $this->belongsToMany('App\Product');
    }
    protected $fillable = ['name','order_id','address','email','phone','paymentMethod','transaction_Number','status'];
}
