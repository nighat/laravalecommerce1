<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\product;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts = Cart::latest()->paginate(10);
        $productids=Cart::select()->where('session_id','=',$this->guestid)->pluck('product_id');
        $products = Product::find($productids);
        $guestid=$this->guestid;
        return view('cart.index',compact('carts','products','guestid'));
    }
    public function view()
    {
        $carts = Cart::all();
        return view('cart.view',compact('carts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cart.cart');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * TODO:UPDATE THE Query to get the count directly
         * 0 delete
         */

        $cartCollections = Cart::where([['session_id','=',$request->session_id],['product_id','=',$request->product_id]])->get();
        if($cartCollections->count()>0){
            $cartCollection = Cart::where([['session_id','=',$request->session_id],['product_id','=',$request->product_id]])->first();
            $id=$cartCollection->id;
            $totalquantity= $cartCollection->qty+$request->qty;
            $totalamount=$totalquantity*$request->price;
            $cart = $request->all();
            $cart['price']=$totalamount;
            $cart['qty']=$totalquantity;
            $cart = Cart::find($id);
            $cart->save() ;;
            return redirect(route('cart.index'));


        }else{
            
            $cart = new Cart;
            $cart->session_id = $request->session_id;
            $cart->product_id = $request->product_id;
            $cart->qty  = $request->qty ;
            $cart->price  = $request->price*$request->qty ;
            $cart->save() ;
        }


        return redirect(route('cart.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cart = Cart::find($id);
        return view('cart.show')->with('cart',$cart);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cart= Cart::where('id',$id)->first();
        return view('cart.edit',compact('cart'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //TODO handle qty 0
        $cart = Cart::find($id);
        //$cart->session_id = $request->session_id;
        //$cart->product_id = $request->product_id;
       $cart->qty  = $request->qty ;
       $cart->price  = $request->price*$request->qty;

       $cart->save();


        return redirect(route('cart.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::where('id',$id)->delete();
        return redirect()->back();
    }
}
