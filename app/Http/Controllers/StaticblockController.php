<?php

namespace App\Http\Controllers;
use App\Staticblock;
use Illuminate\Http\Request;

class StaticblockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staticblocks = Staticblock::latest()->paginate(5);
        return view('staticblock.index',compact('staticblocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('staticblock.staticblock');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
          'title'=>'required',
          'contant'=>'required',
          'created_by'=>'required',
          'updated_by'=>'required',


      ]);
        $staticblocks = new Staticblock;
        $staticblocks->title = $request->title;
        $staticblocks->contant = $request->contant;
        $staticblocks->created_by  = $request->created_by ;
        $staticblocks->updated_by  = $request->updated_by ;
        $staticblocks->save() ;

        return redirect(route('staticblock.index'))->withMessage('Static Block Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staticblock = Staticblock::find($id);
        return view('staticblock.show')->with('staticblock',$staticblock);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staticblock= Staticblock::where('id',$id)->first();
        return view('staticblock.edit',compact('staticblock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'contant'=>'required',
            'created_by'=>'required',
            'updated_by'=>'required',


        ]);
        $staticblocks = Staticblock::find($id);
        $staticblocks->title = $request->title;
        $staticblocks->contant = $request->contant;
        $staticblocks->created_by  = $request->created_by ;
        $staticblocks->updated_by  = $request->updated_by ;
        $staticblocks->save() ;

        return redirect(route('staticblock.index'))->withMessage('Static Block Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Staticblock::where('id',$id)->delete();
        return redirect()->back()->withMessage('Static Block Deleted Successfully');
    }
}
