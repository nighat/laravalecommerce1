<?php

namespace App\Http\Controllers;

use App\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories    = Category::latest()->paginate(5);
        return view('category.index',compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $this->validate($request,[
//
//            'title'=>'required',
//            'created_by'=>'required',
//            'updated_by'=>'required',
//
//        ]);

        $categories  = new Category;
        $categories->title = $request->title;
        if ($request->hasFile('image')) {
            $uploadPath = public_path('/uploads/category');
            $extension = $request->image->getClientOriginalExtension();
            $timestamp = str_replace([' ', ':'], '--', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
            $fileName = $timestamp . '.' . $extension;
            $request->image->move($uploadPath, $fileName);
            $categories->image = $fileName;
        }else{
            $categories->image = NULL;
        }
        $categories ->created_by= $request->created_by;
        $categories ->updated_by = $request->updated_by;
        $categories ->save();

        return redirect(route('category.index'))->withMessage('Category Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category= Category::find($id);
        return view('category.show')->with('category',$category);
    }

    public function view()
    {
        $categories = Category::latest()->paginate(6);
        return view('category.view',compact('categories'));

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::where('id',$id)->first();
        return view('category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $this->validate($request,[
//
//            'title'=>'required',
//            'created_by'=>'required',
//            'updated_by'=>'required',
//
//        ]);

        $category = Category::find($id);
        $category->title = $request->title;
        if ($request->hasFile('image')) {
            $uploadPath = public_path('/uploads/category');
            $extension = $request->image->getClientOriginalExtension();
            $timestamp = str_replace([' ', ':'], '--', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
            $fileName = $timestamp . '.' . $extension;
            $request->image->move($uploadPath, $fileName);
            $category->image = $fileName;
        }else{
            $category->image = NULL;
        }
        $category->created_by= $request->created_by;
        $category->updated_by= $request->updated_by;
        $category->save();

        return redirect(route('category.index'))->withMessage('Category Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::where('id',$id)->delete();
        return redirect()->back()->withMessage('Category Deleted Successfully');
    }
}
