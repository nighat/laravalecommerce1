<?php

namespace App\Http\Controllers;
use App\Slider;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $sliders = Slider::latest()->paginate(5);
        return view('sliders.index',compact('sliders'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sliders.sliders');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $this->validate($request,[
//            'image'=>'required',
//            'contant'=>'required',
//            'link'=>'required',
//            'html_blog'=>'required',
//            'created_by'=>'required',
//            'updated_by'=>'required',
//
//
//        ]);
//        if($request->hasFile('image'))
//        {
//           $image = $request->file('image')->store('public/image');
//
//
//        }

        $slider = new Slider;
        if ($request->hasFile('image')) {
            $uploadPath = public_path('/uploads/sliders');
            $extension = $request->image->getClientOriginalExtension();
            $timestamp = str_replace([' ', ':'], '--', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
            $fileName = $timestamp . '.' . $extension;
            $request->image->move($uploadPath, $fileName);
            $slider->image = $fileName;
        }else{
            $slider->image = NULL;
        }
        //$slider->image = 'image/'.basename($image);
        $slider->contant = $request->contant;
        $slider->link = $request->link;
        $slider->html_blog = $request->html_blog;
        $slider->created_by = $request->created_by;
        $slider->updated_by = $request->updated_by;
        $slider->save();


        return redirect(route('sliders.index'))->withMessage('Slider Added');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slider = Slider::find($id);
        return view('sliders.show')->with('sliders',$slider);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::where('id',$id)->first();
        return view('sliders.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $this->validate($request,[
//
//            'image'=>'required',
//            'contant'=>'required',
//            'link'=>'required',
//            'html_blog'=>'required',
//            'created_by'=>'required',
//            'updated_by'=>'required',
//
//        ]);
//        if($request->hasFile('image'))
//        {
//
//            $image =  $request->image->store('public/image');
//        }
        $slider = Slider::find($id);
        if ($request->hasFile('image')) {
            $uploadPath = public_path('/uploads/sliders');
            $extension = $request->image->getClientOriginalExtension();
            $timestamp = str_replace([' ', ':'], '--', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
            $fileName = $timestamp . '.' . $extension;
            $request->image->move($uploadPath, $fileName);
            $slider->image = $fileName;
        }else{
            $slider->image = NULL;
        }
       // $slider->image = 'image/'.basename($image);
        $slider->contant= $request->contant;
        $slider->link = $request->link;
        $slider->html_blog = $request->html_blog;
        $slider->created_by = $request->created_by;
        $slider->updated_by = $request->updated_by;
        $slider->save();

        return redirect(route('sliders.index'))->withMessage('Sliders Is Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::where('id',$id)->delete();
        return redirect()->back()->withMessage('Slider Is Deleted');
    }
}
