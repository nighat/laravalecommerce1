<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::latest()->paginate(5);
        return view('review.index',compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('review.review');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
           'detail'=>'required',
           'product_id'=>'required',
           'created_by'=>'required',
           'updated_by'=>'required',

       ]);

        $review = new Review;
        $review->detail = $request->detail;
        $review->product_id = $request->product_id;
        $review->created_by = $request->created_by;
        $review->updated_by= $request->updated_by;
        $review->save();


        return redirect(route('review.index'))->withMessage('Review Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = Review::find($id);
        return view('review.show')->with('review',$review);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = Review::where('id',$id)->first();
        return view('review.edit',compact('review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'detail'=>'required',
            'product_id'=>'required',
            'created_by'=>'required',
            'updated_by'=>'required',

        ]);

        $review = Review::find($id);
        $review->detail = $request->detail;
        $review->product_id = $request->product_id;
        $review->created_by = $request->created_by;
        $review->updated_by= $request->updated_by;
        $review->save();

        return redirect(route('review.index'))->withMessage('Reviews is Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Review::where('id',$id)->delete();
        return redirect()->back()->withMessage('Review Is Deleted');
    }
}
