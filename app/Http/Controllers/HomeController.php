<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Product;
use App\Review;
use App\Page;
use App\Cart;
use App\Category;
use App\Staticblock;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $sliders = Slider::all();
        $products = Product::all();
        $reviews = Review::all();
        $staticblocks = Staticblock::all();
        $carts = Cart::all();
        $categories = Category::all();

        return view('home',compact('sliders','products','reviews','staticblocks','carts','categories'));
    }


}
