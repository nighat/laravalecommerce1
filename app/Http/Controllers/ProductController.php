<?php

namespace App\Http\Controllers;
use App\Product;
use App\Cart;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(5);
        return view('product.index',compact('products'));

    }

    public function view()
    {

//        $current = Carbon::now();
//
//      // add 30 days to the current time
//        $trialExpires = $current->addDays(30);
        $products = Product::latest()->paginate(6);
        return view('product.view',compact('products','trialExpires'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('product.product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
           'image'=>'required',
           'title'=>'required',
           'short_description'=>'required',
            'description'=>'required',
            'additional_information'=>'required',
            'price'=>'required',
            'special_price'=>'required',
            'qty'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'sku'=>'required',
            'meta_keyword'=>'required',
            'meta_description'=>'required',
            'product_url'=>'required',
            'created_by'=>'required',
            'updated_by'=>'required',
       ]);
//        if($request->hasFile('image'))
//        {
//            $image = $request->file('image')->store('public/image');
//
//
//        }
        $product = new Product;
        if ($request->hasFile('image')) {
            $uploadPath = public_path('/uploads/images');
            $extension = $request->image->getClientOriginalExtension();
            $timestamp = str_replace([' ', ':'], '--', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
            $fileName = $timestamp . '.' . $extension;
            $request->image->move($uploadPath, $fileName);
            $product->image = $fileName;
        }else{
            $product->image = NULL;
           }


        $product->title = $request->title;
        $product->short_description = $request->short_description;
        $product->description = $request->description;
        $product->additional_information = $request->additional_information;
        $product->price = $request->price;
        $product->special_price = $request->special_price;
        $product->qty = $request->qty;
        $product->start_date = $request->start_date;
        $product->end_date = $request->end_date;
        $product->sku= $request->sku;
        $product->meta_keyword = $request->meta_keyword;
        $product->meta_description = $request->meta_description;
        $product->product_url = $request->product_url;
        $product->created_by = $request->created_by;
        $product->updated_by= $request->updated_by;
        $product->save();


        return redirect(route('product.index'))->withMessage('Product Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $product = Product::find($id);
        return view ('product.show')->with('product',$product);
    }


    public function detail($id)
    {

        $product = Product::find($id);

       // $product = Product::with('cart')->where('id',$id)->first();
       // $cart = Cart::all();

        return view('product.detail')->with('product',$product);
       // return view('product.detail',compact('cart','product'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $product = Product::where('id',$id)->first();
        return view('product.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'image'=>'required',
            'title'=>'required',
            'short_description'=>'required',
            'description'=>'required',
            'additional_information'=>'required',
            'price'=>'required',
            'special_price'=>'required',
            'qty'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'sku'=>'required',
            'meta_keyword'=>'required',
            'meta_description'=>'required',
            'product_url'=>'required',
            'created_by'=>'required',
            'updated_by'=>'required',
        ]);
//        if($request->hasFile('image'))
//        {
//
//            $image =  $request->image->store('public/image');
//        }

        $product = Product::find($id);
        if ($request->hasFile('image')) {
            $uploadPath = public_path('/uploads/images');
            $extension = $request->image->getClientOriginalExtension();
            $timestamp = str_replace([' ', ':'], '--', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
            $fileName = $timestamp . '.' . $extension;
            $request->image->move($uploadPath, $fileName);
            $product->image = $fileName;
        }else{
            $product->image = NULL;
        }
        //$product->image = 'image/'.basename($image);
        $product->title = $request->title;
        $product->short_description = $request->short_description;
        $product->description = $request->description;
        $product->additional_information = $request->additional_information;
        $product->price = $request->price;
        $product->special_price = $request->special_price;
        $product->qty = $request->qty;
        $product->start_date = $request->start_date;
        $product->end_date = $request->end_date;
        $product->sku= $request->sku;
        $product->meta_keyword = $request->meta_keyword;
        $product->meta_description = $request->meta_description;
        $product->product_url = $request->product_url;
        $product->created_by = $request->created_by;
        $product->updated_by= $request->updated_by;
        $product->save();

        return redirect(route('product.index'))->withMessage('Product Updated Successfully');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id',$id)->delete();
        return redirect()->back()->withMessage('Product Deleted Successfully');;
    }
}
