<?php

namespace App\Http\Controllers;

use App\Page;
use App\Product;
use App\Cart;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\View\View;
use Illuminate\Support\Facades\Session;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



      public $guestid=null;
    public function __construct()
    {

        session_start();

       if(array_key_exists('session_id',$_SESSION)){

           $this->guestid =$_SESSION['session_id'];
       }

        if (is_null($this->guestid))
       {
           $_SESSION['session_id']=time().rand();

        }






        //its just a dummy data object.
        $pages = Page::all();

        // Sharing is caring
        \View::share('pages', $pages);

        $carts = Cart::all();

        \View::share('carts', $carts);
         $products = Product::all();

        \View::share('products', $products);




        $id=$this->guestid;
        $productids=Cart::select ()->where('session_id',$id)->pluck('product_id');
        $cartItems = Product::find($productids);
        \View::share('cartItems', $cartItems);

//        dd($products);
        \View::share('id',$id);



    }
}
