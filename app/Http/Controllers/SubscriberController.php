<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscriber;
class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $subscribers = Subscriber::latest()->paginate(5);
        return view('subscriber.index',compact('subscribers'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subscriber.subscriber');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'mail'=>'required',

        ]);

        $subscriber = new Subscriber;
        $subscriber->mail = $request->mail;
        $subscriber->save();


        return redirect(route('subscriber.index'))->withMessage('Subscriber Added');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subscriber = Subscriber::find($id);
        return view('subscriber.show')->with('subscriber',$subscriber);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscriber = Subscriber::where('id',$id)->first();
        return view('subscriber.edit',compact('subscriber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[

            'mail'=>'required',

        ]);

        $subscriber = Subscriber::find($id);
        $subscriber->mail= $request->mail;
        $subscriber->save();

        return redirect(route('subscriber.index'))->withMessage('Subscriber Is Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subscriber::where('id',$id)->delete();
        return redirect()->back()->withMessage('Subscriber Is Deleted');
    }
}
