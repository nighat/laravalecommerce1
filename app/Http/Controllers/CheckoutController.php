<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Checkout;
use App\Cart;
use App\Product;
use App\OrderItem;
use App\Mail\OrderConfirmed;
use Illuminate\Support\Facades\DB;


class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {

        $checkouts = Checkout::latest()->paginate(5);
        return view('checkout.index',compact('checkouts'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $carts = Cart::all();
        $productids=Cart::select()->where('session_id','=',$this->guestid)->pluck('product_id');
        $products = Product::find($productids);
        $guestid=$this->guestid;
        return view('checkout.checkout',compact('carts','products','guestid'));
    }
      public function message()
     {
        return view("checkout.message");
     }

    public function download($orderItems)
     {

         $productids=OrderItem::select()->where('order_id','=',$orderItems)->pluck('product_id');
         $products=Product::find( $productids);

         $userInfo=Checkout::select()->where('order_id','=',$orderItems)->first();

         $orderItems=OrderItem::select()->where('order_id','=',$orderItems)->get();

         $pdf = PDF::loadView('checkout.download',compact('orderItems','products','userInfo'));
         return $pdf->download('order.pdf');

//        $data= "This is rose";
////         dd($id);
//        $pdf = PDF::loadView('checkout.download', compact('data','cartItems','productInfo','orderItem'));
//        return $pdf->download('order.pdf');
//       // return view("checkout.download");
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'paymentMethod'=>'required',

        ]);
//        if ($this->validator->fails()) {
//            return redirect('checkout.create')
//                ->withErrors($this->validator)
//                ->withInput();
//        }

//        if ($_SERVER["REQUEST_METHOD"] == "POST") {
//            if (empty($_POST["name"])) {
//                $checkout->name= "Name is required";
//            } else {
//                $checkout->name =($_POST["name"]);
//            }

        Checkout::create($request->all());
        $cartItems=Cart::select()->where('session_id',$_SESSION['session_id'])->get();
//        dd($cartItems);
        $productId=Cart::select()->where('session_id',$_SESSION['session_id'])->pluck('product_id');
//        dd($productId);
        $productInfo=Product::find($productId);
        foreach ($cartItems as $cart)
        {
            $orderItem['order_id']=$request->order_id;
            $orderItem['product_id']=$cart->product_id;
            $orderItem['qty']=$cart->qty;
            $orderItem['price']=$cart->price;

         OrderItem::create($orderItem);
            //DB:table('order_items')->insert('orderItem')
            $cart->delete();

        }

        $this->sendSMS($request->phone,"Your order is confirmed");
       \Mail::to($request->email)->send(new OrderConfirmed($cartItems,$productInfo));
        session(['order_id'=>$request->order_id]);
        return redirect(route("checkout.message"));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $checkout = Checkout::find($id);
        return view('checkout.show')->with('checkout',$checkout);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $checkout= checkout::where('id',$id)->first();
        return view('checkout.edit',compact('checkout'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'paymentMethod'=>'required',

        ]);

        $checkout = Checkout::find($id);

        $checkout->status= $request->status;

        $checkout->save();

        return redirect(route('checkout.index'))->withMessage('Checkout Is Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Checkout::where('id',$id)->delete();
        return redirect()->back()->withMessage('Checkout Is Deleted');
    }


    public function sendSMS($phone, $message)
    {



        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.infobip.com/sms/1/text/single",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"from\":\"InfoSMS\", \"to\":\"$phone\", \"text\":\"$message\" }",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "authorization: Basic YmFyY29kZXNvbHV0aW9uOkxORUsxYXg0eDEy",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
//    if ($err) {
//        echo "cURL Error #:" . $err;
//    } else {
//        echo $response;
//    }
//    die();
//
//
    }
}
