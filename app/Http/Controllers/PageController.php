<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pages = Page::latest()->paginate(5);
        return view('page.index',compact('pages'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.page');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'page_title'=>'required',
            'page_contant'=>'required',
            'created_by'=>'required',
            'updated_by'=>'required',

        ]);

        $page = new Page;
        $page->page_title = $request->page_title;
        $page->page_contant = $request->page_contant;
        $page->created_by = $request->created_by;
        $page->updated_by = $request->updated_by;
        $page->save();


        return redirect(route('page.index'))->withMessage('Page Added');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::find($id);
        return view('page.show')->with('page',$page);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $page = Page::find($id);
        return view('page.detail')->with('page',$page);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::where('id',$id)->first();
        return view('page.edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[

            'page_title'=>'required',
            'page_contant'=>'required',
            'created_by'=>'required',
            'updated_by'=>'required',

        ]);

        $page = Page::find($id);
        $page->page_title = $request->page_title;
        $page->page_contant = $request->page_contant;
        $page->created_by = $request->created_by;
        $page->updated_by = $request->updated_by;
        $page->save();

        return redirect(route('page.index'))->withMessage('Page Is Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Page::where('id',$id)->delete();
        return redirect()->back()->withMessage('Page Is Deleted');
    }
}
