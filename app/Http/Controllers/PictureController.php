<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Picture;
class PictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pictures = Picture::latest()->paginate(5);
        return view('picture.index',compact('pictures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('picture.picture');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'path'=>'required',
            'type'=>'required',
            'product_id'=>'required',
            'created_by'=>'required',
            'updated_by'=>'required',

        ]);

        $picture = new Picture;
        $picture->path = $request->path;
        $picture->type= $request->type;
        $picture->product_id = $request->product_id;
        $picture->created_by = $request->created_by;
        $picture->updated_by= $request->updated_by;
        $picture->save();


        return redirect(route('picture.index'))->withMessage('Picture Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $picture = Picture::find($id);
        return view('picture.show')->with('picture',$picture);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $picture = Picture::where('id',$id)->first();
        return view('picture.edit',compact('picture'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'path'=>'required',
            'type'=>'required',
            'product_id'=>'required',
            'created_by'=>'required',
            'updated_by'=>'required',

        ]);

        $picture = Picture::find($id);
        $picture->path = $request->path;
        $picture->type = $request->type;
        $picture->product_id = $request->product_id;
        $picture->created_by = $request->created_by;
        $picture->updated_by= $request->updated_by;
        $picture->save();

        return redirect(route('picture.index'))->withMessage('Pictures Is Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Picture::where('id',$id)->delete();
        return redirect()->back()->withMessage('Picture Is Deleted');
    }
}
