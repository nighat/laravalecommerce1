<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@index');

Route::get('/pages/detail/{id}', 'PageController@detail')->name('page.detail');
Route::get('/product/detail/{id}', 'ProductController@detail')->name('product.detail');
Route::get('/product/view', 'ProductController@view')->name('product.view');
Route::get('/cart/view', 'CartController@view')->name('cart.view');


Route::resource('orderItem','OrderItemController');//resource route
Route::resource('cart','CartController');//resource route
Route::resource('sliders','SliderController');//resource route
//Route::get('/sliders','SliderController@index');
//Route::get('/sliders.create','SliderController@create');
//Route::get('/slider-detail/{id}','SliderController@show');
Route::resource('category', 'CategoryController');
Route::get('/category/view', 'CategoryController@view')->name('category.view');
Route::resource('product', 'ProductController');
Route::resource('staticblock', 'StaticblockController');
Route::resource('review', 'ReviewController');
Route::resource('picture', 'PictureController');
Route::resource('subscriber', 'SubscriberController');
Route::resource('page', 'PageController');
Route::resource('checkout', 'CheckoutController');
Route::get('/checkout/message', 'CheckoutController@message')->name('checkout.message');
Route::get('orders/{id}/download', 'CheckoutController@download');
//Route::get('/category','CategoryController@index');
//Route::get('/product','ProductController@index');



//////////////////////////////////

Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', function () {
    return view('about');
});
Route::get('/404', function () {
    return view('404');
});
Route::get('/gallery-post', function () {
    return view('gallery-post');
});
///////////////////
Route::get('/gggallery-post', function () {
    return view('gggallery-post');
});
Route::get('/grid', function () {
    return view('grid');
});
Route::get('/listing-with-sidbar', function () {
    return view('listing-with-sidbar');
});
Route::get('/listing-without-sidbar', function () {
    return view('listing-without-sidbar');
});
Route::get('/masonry', function () {
    return view('masonry');
});
Route::get('/standart-post', function () {
    return view('standart-post');
});
Route::get('/text-post', function () {
    return view('text-post');
});
Route::get('/video-post', function () {
    return view('video-post');
});
Route::get('/cart-1', function () {
    return view('cart-1');
});
Route::get('/cart-2', function () {
    return view('cart-2');
});
Route::get('/checkout-1', function () {
    return view('checkout-1');
});
Route::get('/checkout-2', function () {
    return view('checkout-2');
});
Route::get('/coming-soon', function () {
    return view('coming-soon');
});
Route::get('/compare', function () {
    return view('compare');
});
Route::get('/contacts', function () {
    return view('contacts');
});
Route::get('/elements-accordion', function () {
    return view('elements-accordion');
});
Route::get('/elements-banners', function () {
    return view('elements-banners');
});
Route::get('/elements-buttons', function () {
    return view('elements-buttons');
});
Route::get('/elements-icon-box', function () {
    return view('elements-icon-box');
});
Route::get('/elements-maps', function () {
    return view('elements-maps');
});
Route::get('/elements-product', function () {
    return view('elements-product');
});
Route::get('/elements-progress-bar', function () {
    return view('elements-progress-bar');
});
Route::get('/elements-social', function () {
    return view('elements-social');
});
Route::get('/elements-tabs', function () {
    return view('elements-tabs');
});
Route::get('/elements-toggles', function () {
    return view('elements-toggles');
});
Route::get('/elements-typography', function () {
    return view('elements-typography');
});
Route::get('/empty-category', function () {
    return view('empty-category');
});
Route::get('/empty-compare', function () {
    return view('empty-compare');
});
Route::get('/empty-search', function () {
    return view('empty-search');
});
Route::get('/empty-shopping-cart', function () {
    return view('empty-shopping-cart');
});
Route::get('/empty-wishlist', function () {
    return view('empty-wishlist');
});
Route::get('/faq', function () {
    return view('faq');
});
Route::get('/gallery', function () {
    return view('gallery');
});
Route::get('/gallery-3-thumbs', function () {
    return view('gallery-3-thumbs');
});
Route::get('/gallery-4-thumbs', function () {
    return view('gallery-4-thumbs');
});
Route::get('/gallery-masonry', function () {
    return view('gallery-masonry');
});
Route::get('/index', function () {
    return view('index');
});
Route::get('/index-02', function () {
    return view('index-02');
});
Route::get('/index-03', function () {
    return view('index-03');
});
Route::get('/index-04', function () {
    return view('index-04');
});
Route::get('/index-05', function () {
    return view('index-05');
});
Route::get('/index-06', function () {
    return view('index-06');
});
Route::get('/index-07', function () {
    return view('index-07');
});
Route::get('/index-08', function () {
    return view('index-08');
});
Route::get('/index-09', function () {
    return view('index-09');
});
Route::get('/index-10', function () {
    return view('index-10');
});
Route::get('/index-11', function () {
    return view('index-11');
});
Route::get('/index-12', function () {
    return view('index-12');
});
Route::get('/listing', function () {
    return view('listing');
});
Route::get('/listing-big-reviews', function () {
    return view('listing-big-reviews');
});
Route::get('/listing-catalogue', function () {
    return view('listing-catalogue');
});
Route::get('/listing-collections', function () {
    return view('listing-collections');
});
Route::get('/listing-fullwidth', function () {
    return view('listing-fullwidth');
});
Route::get('/listing-right-sidebar', function () {
    return view('listing-right-sidebar');
});
Route::get('/listing-with-custom-html-block', function () {
    return view('listing-with-custom-html-block');
});
Route::get('/listing-without-columns', function () {
    return view('listing-without-columns');
});
Route::get('/listing-with-sidebar', function () {
    return view('listing-with-sidebar');
});
Route::get('/login', function () {
   return view('login');
});


Route::get('/my-account', function () {
    return view('my-account');
});
//Route::get('/product', function () {
   // return view('product');
//});
Route::get('/product-grouped', function () {
    return view('product-grouped');
});
Route::get('/product-on-sale', function () {
    return view('product-on-sale');
});
Route::get('/product-out-of-stock', function () {
    return view('product-out-of-stock');
});

Route::get('/product-simple-variant-1', function () {
    return view('product-simple-variant-1');
});
Route::get('/product-simple-variant-2', function () {
    return view('product-simple-variant-2');
});
Route::get('/product-variable', function () {
    return view('product-variable');

});
Route::get('/register', function () {
    return view('register');
});
//Route::get('/services', function () {
   // return view('services');
//});
//Route::get('/sitemap', function () {
    //return view('sitemap');
//});
//Route::get('/wishlist', function () {
   // return view('wishlist');
//});

Auth::routes();

Route::get('/home','HomeController@index')->name('home');

